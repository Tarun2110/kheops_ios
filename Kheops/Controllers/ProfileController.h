//
//  ProfileController.h
//  kheops
//
//  Created by Tarun Sharma on 08/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ProfileController : BaseViewController<UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UITextFieldDelegate>
{
    UIImagePickerController *picker;
    NSString *base64String;
    
}
@property (strong, nonatomic) IBOutlet UIButton *DoneButton;
@property (strong, nonatomic) IBOutlet UITextField *NameTextField;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UIButton *ChangePasswordButton;
@property (strong, nonatomic) IBOutlet UITextField *EmailTextfield;
@property (strong, nonatomic) IBOutlet UIButton *EditButton;
@property (strong, nonatomic) IBOutlet UIButton *SubmitButton;
@property (strong, nonatomic) IBOutlet UIButton *AddpictureButton;
@property (strong, nonatomic) IBOutlet UILabel *profilelabel;
@property (strong, nonatomic) IBOutlet UIButton *languageButton;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;

@end
