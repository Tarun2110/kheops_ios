//
//  LocationDetailController.m
//  kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//


#import <GoogleAnalytics/GAITracker.h>
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LocationDetailController.h"
#import <GoogleAnalytics/GAITracker.h>
#import <GAITracker.h>
#import <GAI.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <AFNetworking.h>
#import "AFDataManager.h"
#import "LocalizationSystem.h"
#import "ProfileBagdeController.h"
#import "CollectionViewCell.h"
#import "FriendsViewController.h"
#import "ProfileBagdeController.h"
#import "AppDelegate.h"
//#import "LCBannerView.h"
#import "PECropViewController.h"


@interface LocationDetailController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, PECropViewControllerDelegate>{
    float progressTime;
    float startTime;
    float endTime;
    float totalTime;
    id timeObserver;
    NSURL *soundUrl;
    NSTimer *timer;
    NSMutableArray *FriendsArray, *bannerImagesArray, *imageURLs, *nameArray ;
    int hours;
    int minutes;
    int seconds;
    UIImage *Selectimage;
    UIImagePickerController *picker;
    BOOL imageSelected;
    NSData *SelectedImageData;
    UITapGestureRecognizer *tapGesture1;
    UIScrollView  *scrolview;
    UIPageControl *pager;
}
@property (nonatomic) UIPopoverController *popover;
@end
BOOL Click,Mute,Tapped,screenRotate;

@import MediaPlayer;
@implementation LocationDetailController
@synthesize pin, placeId;

# pragma mark  View Setup
- (void)viewDidLoad{
    [super viewDidLoad];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    Click = FALSE;
    Mute = FALSE;
    _seekBar.minimumValue = 0.0;
    _seekBar.maximumValue = 1.0;
    _friendsCollectionView.delegate = self;
    _friendsCollectionView.dataSource = self;
    
    bannerImagesArray = [NSMutableArray new];
    nameArray = [NSMutableArray new];
    imageURLs = [NSMutableArray new];
    FriendsArray = [NSMutableArray new];
    
    imageSelected = false;
    Tapped = false;
    screenRotate = false;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromHistory"]){
        ShowProgressHUD;
        NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults ]valueForKey:@"selectedLang"];
        [self.apiService API_PlaceDetail:self withSelector:@selector(outputPlaceDetail:) :self.view :selectedLanguage :placeId];
    }
    else{
        _labelheader.text = pin.pinTitle;
        
        if (![pin.pinAudioPath  isEqual: @""]) {
            audiofile = [NSString stringWithFormat:@"%@",pin.pinAudioPath];
            audiofile = [audiofile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        }
        else{
            [AFDataManager showAlertTitle:JDLocalizedString(@"Error") alert:JDLocalizedString(@"No audio file available") onView:self];
        }
        
        NSString *popup = _popUp;
        if ([popup isEqualToString:@"1"]){
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
            imageView.contentMode=UIViewContentModeCenter;
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:_popUpImage]];
            NSString *turndString = JDLocalizedString(@"You turned");
            NSString *inString = JDLocalizedString(@"in");
            
            NSString *messageString = [NSString stringWithFormat:@"%@ %@ %@ %@",turndString,_popUpAward,inString,_popUpPlace];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:JDLocalizedString(@"Congratulations !")
                                                                message:messageString
                                                               delegate:self
                                                      cancelButtonTitle:@"Profile"
                                                      otherButtonTitles:@"Ok", nil];
            
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
                [alertView setValue:imageView forKey:@"accessoryView"];
            }else{
                [alertView addSubview:imageView];
            }
            [alertView show];
        }
        else{
            [self downloadMp3File:audiofile];
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification  object:nil];
}

- (void) outputPlaceDetail:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        
        _labelheader.text = [NSString stringWithFormat:@"%@",[[[responseDict valueForKey:@"result"] valueForKey:@"name"]objectAtIndex:0]];
        
        NSString *htmlString = [NSString stringWithFormat:@"%@",[[[responseDict valueForKey:@"result"] valueForKey:@"description"]objectAtIndex:0]];
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        _Description.attributedText = attributedString;
        
        
        audiofile =  [NSString stringWithFormat:@"%@",[[[responseDict valueForKey:@"result"] valueForKey:@"file"]objectAtIndex:0]];
        audiofile = [audiofile stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        bannerImagesArray = [[[responseDict valueForKey:@"result"] valueForKey:@"images"] objectAtIndex:0];
        
        for (int i = 0; i < bannerImagesArray.count; i++) {
            NSString *imageString = [NSString stringWithFormat:@"%@",[[bannerImagesArray valueForKey:@"image"]objectAtIndex:i]];
            imageString = [imageString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [imageURLs addObject:imageString];
            NSString *userName = [NSString stringWithFormat:@"%@",[[bannerImagesArray valueForKey:@"name"]objectAtIndex:i]];
            
            if ([userName isEqualToString:@""]) {
                userName = @"";
            }
            else{
                NSString *stringAttach = JDLocalizedString(@"Photo by");
                userName = [NSString stringWithFormat:@"%@ %@",stringAttach,userName];
            }
            [nameArray addObject:userName];
        }
        FriendsArray = [[[responseDict valueForKey:@"result"] valueForKey:@"friend_list"] objectAtIndex:0];
        
        if (FriendsArray.count>0){
            [_friendsCollectionView reloadData];
            [_friendsCollectionView setHidden:NO];
            [_emptyArrayView setHidden:YES];
        }
        else{
            [_friendsCollectionView setHidden:YES];
            [_emptyArrayView setHidden:NO];
            _firstLabel.text = JDLocalizedString(@"You're the first of your eagles to unlock this spot !");
            [self.addFriendsLinkButton setTitle:JDLocalizedString(@"+ Add eagles") forState:UIControlStateNormal];
        }
        [self downloadMp3File:audiofile];
    }
}

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            screenRotate = false;
            if ( Tapped == false){
                Tapped = true;
                [UIView beginAnimations: @"Fade Out" context:nil];
                [UIView setAnimationDelay:0.0f];
                [UIView setAnimationDuration:1.0f];
                [UIView commitAnimations];
                headerHeightInLandscapeConstant.constant = 0;
                seekViewHeightConstrainInLanscape.constant = 0;
            }
            
            [self viewDidAppear:YES];
            [_Pintitlelabel setHidden:NO];
            _backButton.alpha = 1.0;
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            screenRotate = true;
            Tapped = false;
            headerHeightInLandscapeConstant.constant = 50;
            seekViewHeightConstrainInLanscape.constant = 50;
            
            [self viewDidAppear:YES];
            [_Pintitlelabel setHidden:YES];
            _backButton.alpha = 0.0;
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

-(BOOL)shouldAutorotate{
    return NO;
}

-(void) restrictRotation:(BOOL) restriction{
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}


-(void)viewWillAppear:(BOOL)animated
{
    [self restrictRotation:YES];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromHistory"]){
        
    }
    else{
        bannerImagesArray = [NSMutableArray new];
        nameArray = [NSMutableArray new];
        imageURLs = [NSMutableArray new];
        FriendsArray = [NSMutableArray new];
        
        if (imageSelected == FALSE){
            bannerImagesArray = pin.imagesArray;
            
            for (int i = 0; i < bannerImagesArray.count; i++) {
                NSString *imageString = [NSString stringWithFormat:@"%@",[[bannerImagesArray valueForKey:@"image"]objectAtIndex:i]];
                imageString = [imageString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [imageURLs addObject:imageString];
                
                NSString *userName = [NSString stringWithFormat:@"%@",[[bannerImagesArray valueForKey:@"name"]objectAtIndex:i]];
                
                if ([userName isEqualToString:@""]) {
                    userName = @"";
                }
                else{
                    NSString *stringAttach = JDLocalizedString(@"Photo by");
                    userName = [NSString stringWithFormat:@"%@ %@",stringAttach,userName];
                }
                [nameArray addObject:userName];
            }
            
            FriendsArray = pin.friendsListArray;
            
            if (FriendsArray.count>0){
                [_friendsCollectionView setHidden:NO];
                [_emptyArrayView setHidden:YES];
            }
            else{
                [_friendsCollectionView setHidden:YES];
                [_emptyArrayView setHidden:NO];
                _firstLabel.text = JDLocalizedString(@"You're the first of your eagles to unlock this spot !");
                [self.addFriendsLinkButton setTitle:JDLocalizedString(@"+ Add eagles") forState:UIControlStateNormal];
            }
            NSString *htmlString = pin.pinDesc;
            NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                    initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                    options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                    documentAttributes: nil
                                                    error: nil
                                                    ];
            _Description.attributedText = attributedString;
            _Pintitlelabel.text = @"";
        }
    }
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"LocationDetail Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ReloadPause:) name:@"TogglePause" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ReloadPlay:) name:@"TogglePlay" object:nil];
}


-(void)viewDidAppear:(BOOL)animated{
    [self performSelector:@selector(bannerViewDisplay) withObject:nil afterDelay:0.2f];
}

-(void)bannerViewDisplay{
    for (UIView *view in _BannerView.subviews) {
        [view removeFromSuperview];
    }
    pager = [[UIPageControl alloc] initWithFrame:CGRectMake(self.BannerView.frame.size.width/2- 25, self.BannerView.frame.size.height - 50, 50, 50)];
    pager.numberOfPages = imageURLs.count;
    pager.pageIndicatorTintColor = [UIColor grayColor];
    pager.currentPageIndicatorTintColor = [UIColor yellowColor];
    
    int xAxis = 0;
    scrolview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.BannerView.frame.size.width, self.BannerView.frame.size.height)];
    scrolview.pagingEnabled = YES;
    scrolview.delegate = self;
    pager.currentPage = 0;
    
    for (int i = 0; i < imageURLs.count ; i++){
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xAxis, 0, scrolview.frame.size.width, scrolview.frame.size.height)];
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(xAxis+20 , 20, 200, 20)];
        nameLabel.text = nameArray[i];
        nameLabel.textColor = [UIColor whiteColor];
        [nameLabel setFont:[UIFont fontWithName:@"Museo Sans W01 Rounded 300_0" size:14]];
        nameLabel.textAlignment = NSTextAlignmentLeft;
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:imageURLs[i]];
        [scrolview addSubview:imageView];
        [scrolview addSubview:nameLabel];
        xAxis += scrolview.frame.size.width;
        scrolview.contentSize = CGSizeMake(xAxis, scrolview.frame.size.height);
    }
    
    [self.BannerView addSubview:scrolview];
    [self.BannerView addSubview:pager];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(tappedAction_cameraButton:)
     forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(self.BannerView.frame.size.width - 60, self.BannerView.frame.size.height - 60, 40, 40.0);
    UIImage *cameraButton = [UIImage imageNamed:@"camera"];
    [button setImage:cameraButton forState:UIControlStateNormal];
    [_BannerView addSubview:button];
    
    if (screenRotate == true) {
        tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
        tapGesture1.numberOfTapsRequired = 1;
        [tapGesture1 setDelegate:self];
        [scrolview addGestureRecognizer:tapGesture1];
    }
}


-(void)tapGesture:(UITapGestureRecognizer*)sender {
    if ( Tapped == false){
        Tapped = true;
        [UIView beginAnimations: @"Fade Out" context:nil];
        [UIView setAnimationDelay:0.0f];
        [UIView setAnimationDuration:1.0f];
        _cameraButton.alpha=0.0;
        [UIView commitAnimations];
        headerHeightInLandscapeConstant.constant = 0;
        seekViewHeightConstrainInLanscape.constant = 0;
    }
    else{
        Tapped = false;
        [UIView beginAnimations: @"Fade In" context:nil];
        [UIView setAnimationDelay:0.0f];
        [UIView setAnimationDuration:1.0f];
        _cameraButton.alpha = 1.0;
        [UIView commitAnimations];
        headerHeightInLandscapeConstant.constant = 50;
        seekViewHeightConstrainInLanscape.constant = 50;
    }
    [self performSelector:@selector(bannerViewDisplay) withObject:nil afterDelay:0.2f];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromSideMenu"];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ProfileBagdeController * controller = [storyboard instantiateViewControllerWithIdentifier:@"ProfileBagdeController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (buttonIndex == 1){
        [alertView dismissWithClickedButtonIndex:1 animated:TRUE];
        [self downloadMp3File:audiofile];
    }
}

# pragma mark  Audio Functions

-(void)downloadMp3File:(NSString *)file{
    ShowProgressTextHUD;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:file]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:25.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:@{@"Content-Type" : @"application/json",@"Accept" : @"application/json"}];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                                  HideProgressHUD;
                                                  [AFDataManager showAlertTitle:JDLocalizedString(@"Error") alert:error.localizedDescription onView:self];
                                              }];
                                          } else {
                                              [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                                  HideProgressHUD;
                                                  NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
                                                  NSURL *documentsURL = URLs[0];
                                                  NSURL *filePath = [documentsURL URLByAppendingPathComponent:@"audio.mp3"];
                                                  [[NSFileManager defaultManager] createDirectoryAtURL:filePath withIntermediateDirectories:true attributes:nil error:nil];
                                                  [[NSFileManager defaultManager] removeItemAtURL:filePath error:nil];
                                                  [data writeToURL:filePath atomically:true];
                                                  [self getAudioPath];
                                              }];
                                          }
                                      }];
    [dataTask resume];
}


- (void) ReloadPause:(NSNotification *)notification{
    [_playera pause];
}

- (void) ReloadPlay:(NSNotification *)notification{
    [_playera play];
}


-(void) getAudioPath{
    NSString *filename = @"audio.mp3";
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    NSString *documentsDirectory = [pathArray objectAtIndex:0];
    NSString *yourSoundPath = [documentsDirectory stringByAppendingPathComponent:filename];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:yourSoundPath]){
        soundUrl = [NSURL fileURLWithPath:yourSoundPath isDirectory:NO];
    }
    [self setUpVideoPlayer];
}


-(void)setUpVideoPlayer{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *aErr;
    [audioSession setCategory:AVAudioSessionCategoryAmbient withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&aErr];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    NSError *setCategoryError = nil;
    if (![session setCategory:AVAudioSessionCategoryPlayback
                  withOptions:AVAudioSessionCategoryOptionMixWithOthers
                        error:&setCategoryError])
    {
    }
    _playera = [AVPlayer playerWithURL:soundUrl];
    _avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:_playera];
    [_avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    _avPlayerLayer.frame = CGRectMake(0, 0, _containerView.frame.size.width, _containerView.frame.size.height);
    
    _containerView.backgroundColor = [UIColor whiteColor];
    [_containerView.layer addSublayer:_avPlayerLayer];
    [_playera setVolume:1.0];
    [_playera seekToTime:kCMTimeZero];
    [_playera setActionAtItemEnd:AVPlayerActionAtItemEndNone];
    
    CMTime duration = _playera.currentItem.asset.duration;
    totalTime = CMTimeGetSeconds(duration);
    
    int TotalTime = CMTimeGetSeconds(duration);
    seconds = TotalTime % 60;
    minutes = (TotalTime / 60) % 60;
    hours = TotalTime / 3600;
    
    _TotalTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d",minutes, seconds];
    
    [_seekBar addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[_playera currentItem]];
    
    void (^observerBlock)(CMTime time) = ^(CMTime time){
        NSString *timeString = [NSString stringWithFormat:@"%02.2f", (float)time.value / (float)time.timescale];
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
            NSLog(@"%@",timeString);
        }
        else{
            NSLog(@"App is backgrounded. Time is: %@", timeString);
        }
    };
    __weak typeof(self) weakSelf = self;
    
    timeObserver = [self.playera addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:dispatch_get_main_queue() usingBlock:^(CMTime time)
                    {
                        [weakSelf observeTime:time];
                    }];
    
    MPNowPlayingInfoCenter *playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
    MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:[UIImage imageNamed:@"00_splash"]];
    
    [songInfo setObject:@"Kheops Audio" forKey:MPMediaItemPropertyTitle];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"fromHistory"])
    {
        [songInfo setObject:pin.pinTitle forKey:MPMediaItemPropertyAlbumTitle];
    }
    [songInfo setObject:[NSNumber numberWithDouble:totalTime] forKey:MPMediaItemPropertyPlaybackDuration];
    [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
    [playingInfoCenter setNowPlayingInfo:songInfo];
}



- (IBAction)sliderValueChanged:(UISlider *)sender{
    CMTime time = CMTimeMakeWithSeconds(totalTime/(1/sender.value), totalTime);
    [_playera seekToTime:time];
}

-(void)playerStartPlaying{
    [_playera play];
}


-(void)playerItemDidReachEnd:(NSNotification*)notification{
    Click = false;
    UIImage *btnImage = [UIImage imageNamed:@"btn_play"];
    [_playbutton setImage:btnImage forState:UIControlStateNormal];
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
    [_playera pause];
}

-(void)observeTime:(CMTime)time{
    progressTime = CMTimeGetSeconds(time);
    _seekBar.value = (progressTime/totalTime);
    int TotalTime = CMTimeGetSeconds(time);
    seconds = TotalTime % 60;
    minutes = (TotalTime / 60) % 60;
    hours = TotalTime / 3600;
    _timerLabel.text = [NSString stringWithFormat:@"%02d:%02d",minutes, seconds];
}


# pragma mark  Button Tapped Actions


- (IBAction)tappedAction_cameraButton:(id)sender {
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:JDLocalizedString(@"Select Photo via")
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *CameraAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Camera")
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action)
                                       {
                                           picker = [[UIImagePickerController alloc] init];
                                           picker.delegate = self;
                                           picker.allowsEditing = YES;
                                           picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                           [self presentViewController:picker animated:YES completion:NULL];
                                           
                                       }];
        
        UIAlertAction *LibraryAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Photo Library")
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action)
                                        {
                                            [self openPhotoAlbum];
                                        }];
        
        UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Cancel")
                                                               style:UIAlertActionStyleDestructive
                                                             handler:^(UIAlertAction *action)
                                       {
                                       }];
        [alertController addAction:CameraAction];
        [alertController addAction:LibraryAction];
        [alertController addAction:CancelAction];
        [alertController setModalPresentationStyle:UIModalPresentationCurrentContext];
        
        alertController.popoverPresentationController.sourceView = self.view;
        alertController.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.5, self.view.bounds.size.height / 3.8, 1.0, 1.0);
        
        [self presentViewController:alertController animated:YES
                         completion:nil];
        
    }
}


- (void)openPhotoAlbum
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (self.popover.isPopoverVisible) {
            [self.popover dismissPopoverAnimated:NO];
        }
        self.popover = [[UIPopoverController alloc] initWithContentViewController:controller];
        [self.popover presentPopoverFromBarButtonItem:self.cameraButton
                             permittedArrowDirections:UIPopoverArrowDirectionAny
                                             animated:YES];
    } else {
        [self presentViewController:controller animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    Selectimage = info[UIImagePickerControllerOriginalImage];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (self.popover.isPopoverVisible) {
            [self.popover dismissPopoverAnimated:NO];
        }
        [self openEditor:nil];
    } else {
        [picker dismissViewControllerAnimated:YES completion:^{
            [self openEditor:nil];
        }];
    }
}


- (IBAction)openEditor:(id)sender
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = Selectimage;
    
    UIImage *image = Selectimage;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2,
                                          (height - length) / 2,
                                          length,
                                          length);
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    [self presentViewController:navigationController animated:YES completion:NULL];
}


#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    SelectedImageData = UIImageJPEGRepresentation(croppedImage,0.5);
    imageSelected = TRUE;
    [self updateUserImage];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)TappedAction_MuteButton:(id)sender{
    if (Mute == FALSE){
        Mute = TRUE;
        _playera.muted = true;
        UIImage *btnImage = [UIImage imageNamed:@"icn_mute"];
        [_MuteButton setImage:btnImage forState:UIControlStateNormal];
    }
    else{
        Mute = FALSE;
        _playera.muted = FALSE;
        UIImage *btnImage = [UIImage imageNamed:@"icn_volume"];
        [_MuteButton setImage:btnImage forState:UIControlStateNormal];
    }
}


- (IBAction)TappedAction_MenuButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)TappedAction_playAudio:(id)sender{
    if (Click ==FALSE){
        [_playera play];
        Click = TRUE;
        UIImage *btnImage = [UIImage imageNamed:@"btn_pause"];
        [_playbutton setImage:btnImage forState:UIControlStateNormal];
    }
    else{
        Click = FALSE;
        [_playera pause];
        UIImage *btnImage = [UIImage imageNamed:@"btn_play"];
        [_playbutton setImage:btnImage forState:UIControlStateNormal];
    }}

- (IBAction)TappedAction_AddFriends:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FriendsViewController * controller = [storyboard instantiateViewControllerWithIdentifier:@"FriendsViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

# pragma mark  ImagePicker Delegates

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)updateUserImage
{
    ShowProgressHUD;
    NSString *image = @"image";
    NSString *place_id;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromHistory"]){
        place_id = placeId;
    }
    else{
        place_id = pin.pinId;
    }
    
    NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]];
    NSDictionary *headers = @{
                              @"authorization": token,
                              };
    NSString *urlString = [NSString stringWithFormat:@"http://46.101.79.232/api/v1/places/uploadimage"];
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setAllHTTPHeaderFields:headers];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"place_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",place_id] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if (SelectedImageData){
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"image\"; filename=\"%@\"\r\n",image] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:SelectedImageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [request setHTTPBody:body];
    
    NSURLSessionDataTask * dataTask =[[NSURLSession sharedSession]
                                      dataTaskWithRequest:request
                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                      {
                                          if(data == nil){
                                              return;
                                          }
                                          NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                          jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              HideProgressHUD;
                                              int  successString = [[jsonResponse valueForKey:@"success"] intValue];
                                              if (successString == 1){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      ShowProgressSuccess;
                                                      
                                                      bannerImagesArray = [NSMutableArray new];
                                                      bannerImagesArray = [jsonResponse valueForKey:@"result"];
                                                      nameArray = [NSMutableArray new];
                                                      imageURLs = [NSMutableArray new];
                                                      
                                                      for (int i = 0; i < bannerImagesArray.count; i++) {
                                                          NSString *imageString = [NSString stringWithFormat:@"%@",[[bannerImagesArray valueForKey:@"image"]objectAtIndex:i]];
                                                          imageString = [imageString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                                          [imageURLs addObject:imageString];
                                                          
                                                          NSString *userName = [NSString stringWithFormat:@"%@",[[bannerImagesArray valueForKey:@"name"]objectAtIndex:i]];
                                                          
                                                          if ([userName isEqualToString:@""]) {
                                                              userName = @"";
                                                          }
                                                          else{
                                                              NSString *stringAttach = JDLocalizedString(@"Photo by");
                                                              userName = [NSString stringWithFormat:@"%@ %@",stringAttach,userName];
                                                          }
                                                          [nameArray addObject:userName];
                                                      }
                                                      
                                                      
                                                      
                                                      [self performSelector:@selector(bannerViewDisplay) withObject:nil afterDelay:0.2f];
                                                  });
                                              }
                                              else{
                                              }
                                          });
                                      }];
    [dataTask resume];
}




- (void)remoteControlReceivedWithEvent: (UIEvent *) receivedEvent{
    if (receivedEvent.type == UIEventTypeRemoteControl) {
        switch (receivedEvent.subtype) {
            case UIEventSubtypeRemoteControlStop:
                [_playera pause];
                break;
            case UIEventSubtypeRemoteControlNextTrack:
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
                break;
            case UIEventSubtypeRemoteControlPlay:
            case UIEventSubtypeRemoteControlPause:
            case UIEventSubtypeRemoteControlTogglePlayPause:
                if (_playera.status == AVPlayerStatusReadyToPlay) {
                    [_playera play];
                }else {
                    [_playera pause];
                }
                break;
                
            default: break;
        }}}


# pragma mark  CollectionView Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection: (NSInteger)section{
    return FriendsArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CollectionViewCell *Cell = (CollectionViewCell *)[_friendsCollectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    Cell.contentView.frame = Cell.bounds;
    
    Cell.layer.masksToBounds = NO;
    Cell.layer.borderColor = [UIColor whiteColor].CGColor;
    Cell.layer.borderWidth = 2.0f;
    Cell.layer.contentsScale = [UIScreen mainScreen].scale;
    Cell.layer.shadowOpacity = 0.55f;
    Cell.layer.shadowRadius = 2.0f;
    Cell.layer.shadowOffset = CGSizeZero;
    Cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:Cell.bounds].CGPath;
    Cell.layer.shouldRasterize = YES;
    
    Cell.friendsName.text = [[FriendsArray valueForKey:@"name"] objectAtIndex:indexPath.row] ;
    [Cell.friendsImage sd_setImageWithURL:[NSURL URLWithString:[[FriendsArray valueForKey:@"image"] objectAtIndex:indexPath.row]]
                         placeholderImage:[UIImage imageNamed:@"placedefault.png"]];
    Cell.friendsImage .layer.cornerRadius = Cell.friendsImage.frame.size.width / 2;
    Cell.friendsImage .clipsToBounds = YES;
    Cell.friendsImage .layer.borderWidth = 1.0f;
    Cell.friendsImage .layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    return Cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(106, 106);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(25, 20, 25, 20);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"userProfile"];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSString *friendId =  [[FriendsArray valueForKey:@"id"] objectAtIndex:indexPath.row] ;
    [[NSUserDefaults standardUserDefaults] setValue:friendId forKey:@"freindsID"];
    
    ProfileBagdeController * controller = [storyboard instantiateViewControllerWithIdentifier:@"ProfileBagdeController"];
    [self.navigationController pushViewController:controller animated:YES];
}

# pragma mark  ScrollView Delegates

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;
    pager.currentPage = indexOfPage;
}

-(void)viewDidDisappear:(BOOL)animated{
    [self restrictRotation:NO];
    imageSelected = false;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end


