//
//  HistoryViewController.m
//  Kheops
//
//  Created by Rakesh Kumar on 05/03/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

#import "HistoryViewController.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)tappedAction_menuButton:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSideBar" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
