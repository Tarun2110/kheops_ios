
//
//  FriendsViewController.m
//  Kheops
//
//  Created by Rakesh Kumar on 02/02/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

#import "FriendsViewController.h"
#import "FriendsViewCell.h"
#import "LocalizationSystem.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileBagdeController.h"


@interface FriendsViewController (){
    NSMutableArray *SearchArray;
    NSMutableArray *TotalfriendListArray;
    NSMutableArray *SuggestionListArray;
    __weak IBOutlet UILabel *headerLabel;
    NSMutableArray *RequestListArray;
    NSMutableArray *MyfriendsListArray;
    NSString *fbTokenString;
}
@end
@implementation FriendsViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [_suggestionTableView setHidden:YES];
    [_friendsTableView setHidden:NO];
    [_searchTableView setHidden:YES];
    _searchBar.delegate = self;
    _segmentController.selectedSegmentIndex = 0;
    _searchBar.placeholder = JDLocalizedString(@"Search a friend");
    fbTokenString = [[NSUserDefaults standardUserDefaults] valueForKey:@"fbToken"];
    
    headerLabel.text = JDLocalizedString(@"All Friends");
    [_segmentController setTitle:JDLocalizedString(@"Friends") forSegmentAtIndex:0];
    [_segmentController setTitle:JDLocalizedString(@"Suggestions") forSegmentAtIndex:1];
}

-(void)viewWillAppear:(BOOL)animated{
    TotalfriendListArray = [NSMutableArray new];
    SuggestionListArray = [NSMutableArray new];

    [super viewWillAppear:YES];
    ShowProgressHUD;
    [self.apiService API_FriendList:self withSelector:@selector(outputFriendList:) :self.view :fbTokenString];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

- (void) outputFriendList:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        
        TotalfriendListArray = [[responseDict valueForKey:@"result"] valueForKey:@"friendlist"];
        SuggestionListArray = [[responseDict valueForKey:@"result"] valueForKey:@"suggestion"];
        
        RequestListArray = [NSMutableArray new];
        MyfriendsListArray = [NSMutableArray new];
        
        if (TotalfriendListArray){
            for(int i = 0 ; i < TotalfriendListArray.count ; i++){
                NSString *Requests = [[TotalfriendListArray valueForKey:@"status"] objectAtIndex:i];
                if ([Requests isEqualToString:@"4"]){
                    [RequestListArray addObject:[TotalfriendListArray objectAtIndex:i]];
                }
                else if ([Requests isEqualToString:@"1"]){
                    [MyfriendsListArray addObject:[TotalfriendListArray objectAtIndex:i]];
                }}
        }
        NSArray *sortedArray = [MyfriendsListArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            if ([[obj1 objectForKey:@"places_visit"] floatValue] < [[obj2 objectForKey:@"places_visit"] floatValue])
                return NSOrderedDescending;
            else if ([[obj1 objectForKey:@"places_visit"] floatValue] > [[obj2 objectForKey:@"places_visit"] floatValue])
                return NSOrderedAscending;
            return NSOrderedSame;
        }];
        MyfriendsListArray = [sortedArray mutableCopy];
        HideProgressHUD;
        _friendsTableView.delegate = self;
        _friendsTableView.dataSource = self;
        _suggestionTableView.delegate = self;
        _suggestionTableView.dataSource = self;

        [_suggestionTableView reloadData];
        [_friendsTableView reloadData];
    }
}


- (IBAction)tappedAction_MenuButton:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSideBar" object:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == _friendsTableView){
        return 2;
    }
    else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _friendsTableView){
        if (section==0){
            return RequestListArray.count;
        }
        else{
            return MyfriendsListArray.count;
        }
    }
    else if (tableView == _searchTableView){
        return SearchArray.count;
    }
    else{
        return SuggestionListArray.count;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == _friendsTableView){
        if(section == 0)
            return  JDLocalizedString(@"Friends Requests") ;
        else
            return  JDLocalizedString(@"All Friends");
    }
    else if (tableView == _suggestionTableView){
        return JDLocalizedString(@"Suggestions");
    }
    else{
        return JDLocalizedString(@"Results");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *VideoCellIdentifier = @"FriendsViewCell";
    FriendsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    FriendsViewCell *suggestCell = [tableView dequeueReusableCellWithIdentifier:@"SuggestCell"];
    FriendsViewCell *SearchCell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell"];

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    suggestCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (tableView == _friendsTableView){
        if (indexPath.section==0){
            if (cell == nil){
                cell = [[[NSBundle mainBundle ] loadNibNamed:@"FriendsViewCell" owner:self options:nil]objectAtIndex:0];
                cell.requestNameLabel.text = [[RequestListArray valueForKey:@"name"] objectAtIndex:indexPath.row];
                
                [cell.requestUserImage sd_setImageWithURL:[NSURL URLWithString:[[RequestListArray valueForKey:@"image"] objectAtIndex:indexPath.row]]];
                cell.requestUserImage.layer.cornerRadius = cell.requestUserImage.frame.size.width / 2;
                cell.requestUserImage.clipsToBounds = YES;
                cell.requestUserImage.layer.borderWidth = 1.0f;
                cell.requestUserImage.layer.borderColor = [UIColor lightGrayColor].CGColor;

                cell.AcceptButton.tag = indexPath.row;
                cell.RejectButton.tag = indexPath.row;
                [cell.AcceptButton addTarget:self action:@selector(sendAcceptRequest:) forControlEvents:UIControlEventTouchUpInside];
                [cell.RejectButton addTarget:self action:@selector(sendRejectRequest:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.RejectButton.layer.cornerRadius = cell.RejectButton.frame.size.width / 2;
                cell.RejectButton.clipsToBounds = YES;
                cell.RejectButton.layer.borderWidth = 1.0f;
                cell.RejectButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
                
                cell.AcceptButton.layer.cornerRadius = cell.AcceptButton.frame.size.width / 2;
                cell.AcceptButton.clipsToBounds = YES;
                cell.AcceptButton.layer.borderWidth = 1.0f;
                cell.AcceptButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        }
        else{
            if (cell == nil){
                cell = [[[NSBundle mainBundle ] loadNibNamed:@"FriendsViewCell" owner:self options:nil]objectAtIndex:1];
                cell.friendsNameLabel.text = [[MyfriendsListArray valueForKey:@"name"] objectAtIndex:indexPath.row];
                cell.friendsScore.text=[NSString stringWithFormat:@"%@",[[MyfriendsListArray valueForKey:@"places_visit"] objectAtIndex:indexPath.row]];
                [cell.friendImage sd_setImageWithURL:[NSURL URLWithString:[[MyfriendsListArray valueForKey:@"image"] objectAtIndex:indexPath.row]]];
                NSString* Badgeimage = [[MyfriendsListArray valueForKey:@"award_image"] objectAtIndex:indexPath.row];
                
                if ([Badgeimage isEqualToString:@""]) {
                    [cell.friendsBadgeImage setImage:[UIImage imageNamed:@"logo_login"]];
                }
                else{
                    [cell.friendsBadgeImage sd_setImageWithURL:[NSURL URLWithString:Badgeimage]];
                }
                
                cell.friendImage.layer.cornerRadius = cell.friendImage.frame.size.width / 2;
                cell.friendImage.clipsToBounds = YES;
                cell.friendImage.layer.borderWidth = 1.0f;
                cell.friendImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }}
        return cell;
    }
    else if (tableView == _searchTableView)
    {
        if (SearchCell == nil){
            SearchCell = [[[NSBundle mainBundle ] loadNibNamed:@"FriendsViewCell" owner:self options:nil]objectAtIndex:3];
        }
        SearchCell.searchNameLabel.text = [[SearchArray valueForKey:@"name"] objectAtIndex:indexPath.row];
        [SearchCell.searchUserImage sd_setImageWithURL:[NSURL URLWithString:[[SearchArray valueForKey:@"image"] objectAtIndex:indexPath.row]]];
        SearchCell.searchUserImage.layer.cornerRadius = suggestCell.suggestionUserImage.frame.size.width / 2;
        SearchCell.searchUserImage.clipsToBounds = YES;
        SearchCell.searchUserImage.layer.borderWidth = 1.0f;
        SearchCell.searchUserImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        NSString *friendStatus = [[SearchArray valueForKey:@"status"] objectAtIndex:indexPath.row];
        SearchCell.searchRequestButton.tag = indexPath.row;
        
        if ([friendStatus isEqualToString:@"0"]){
            [SearchCell.searchRequestButton setTitle:JDLocalizedString(@"Request Pending") forState:UIControlStateNormal];
            [SearchCell.searchRequestButton setEnabled:NO];
        }
        else if ([friendStatus isEqualToString:@"1"]){
            [SearchCell.searchRequestButton setTitle:JDLocalizedString(@"Friend") forState:UIControlStateNormal];
            [SearchCell.searchRequestButton setEnabled:NO];
        }
        else{
            [SearchCell.searchRequestButton setTitle:JDLocalizedString(@"Send Request") forState:UIControlStateNormal];
            [SearchCell.searchRequestButton setEnabled:YES];
            [SearchCell.searchRequestButton addTarget:self action:@selector(sendSearchRequestAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        return SearchCell;
    }
    else  {
        if (suggestCell == nil){
            suggestCell = [[[NSBundle mainBundle ] loadNibNamed:@"FriendsViewCell" owner:self options:nil]objectAtIndex:2];
        }
        suggestCell.suggestedFriendNameLabel.text = [[SuggestionListArray valueForKey:@"name"] objectAtIndex:indexPath.row];
        [suggestCell.suggestionUserImage sd_setImageWithURL:[NSURL URLWithString:[[SuggestionListArray valueForKey:@"image"] objectAtIndex:indexPath.row]]];
        suggestCell.suggestionUserImage.layer.cornerRadius = suggestCell.suggestionUserImage.frame.size.width / 2;
        suggestCell.suggestionUserImage.clipsToBounds = YES;
        suggestCell.suggestionUserImage.layer.borderWidth = 1.0f;
        suggestCell.suggestionUserImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        [suggestCell.SendRequestButton setTitle:JDLocalizedString(@"Send Request") forState:UIControlStateNormal];
        [suggestCell.SendRequestButton setEnabled:YES];
        [suggestCell.SendRequestButton addTarget:self action:@selector(sendRequestAction:) forControlEvents:UIControlEventTouchUpInside];
        
        return suggestCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _friendsTableView){
        if (indexPath.section==1){
            NSLog(@"Challeyaaa");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"userProfile"];
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            NSString *friendId =  [[MyfriendsListArray valueForKey:@"id"] objectAtIndex:indexPath.row] ;
            [[NSUserDefaults standardUserDefaults] setValue:friendId forKey:@"freindsID"];
            
            ProfileBagdeController * controller = [storyboard instantiateViewControllerWithIdentifier:@"ProfileBagdeController"];
            [self.navigationController pushViewController:controller animated:YES];

        }
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath;{
    return 70;
}


- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0){
        [_suggestionTableView setHidden:YES];
        [_friendsTableView setHidden:NO];
        [_searchTableView setHidden:YES];
    }
    else{
        [_suggestionTableView setHidden:NO];
        [_friendsTableView setHidden:YES];
       [_searchTableView setHidden:YES];
    }
}

-(void)sendAcceptRequest:(UIButton*)sender{
    NSString *userId = [[RequestListArray valueForKey:@"id"] objectAtIndex:sender.tag] ;
    [self.apiService API_FriendResponse:self withSelector:@selector(outputForAcceptingFriend:) :self.view :userId :@"1"];
}

-(void)sendRejectRequest:(UIButton*)sender{
    NSString *userId = [[RequestListArray valueForKey:@"id"] objectAtIndex:sender.tag] ;
    [self.apiService API_FriendResponse:self withSelector:@selector(outputForRejectingFriend:) :self.view :userId :@"2"];
}

- (void) outputForAcceptingFriend:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Friend Added.") :self];
        [self viewWillAppear:YES];
        
    }
}

- (void) outputForRejectingFriend:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        [self viewWillAppear:YES];

    }
}

-(void)sendSearchRequestAction:(UIButton*)sender{
    NSString *userId = [[SearchArray valueForKey:@"id"] objectAtIndex:sender.tag] ;
    [self.apiService API_AddFriend:self withSelector:@selector(outputAddFriend:) :self.view :userId];
}

-(void)sendRequestAction:(UIButton*)sender{
    NSString *userId = [[SuggestionListArray valueForKey:@"id"] objectAtIndex:sender.tag] ;
    [self.apiService API_AddFriend:self withSelector:@selector(outputAddSuggestedFriend:) :self.view :userId];
}


- (void) outputAddSuggestedFriend:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Request Sent.") :self];
        [self viewWillAppear:YES];
    }
    else{
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Something went wrong.") :self];
    }
}


- (void) outputAddFriend:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Request Sent.") :self];
        [self.apiService API_SearchFriend:self withSelector:@selector(outputSearchFriend:) :self.view :_searchBar.text];
    }
    else{
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Something went wrong.") :self];
    }
}

#pragma mark  SearchBar Delegates

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if ( [searchBar.text length] > 0 ){
        [_searchTableView setHidden:NO];
        [searchBar setShowsCancelButton:YES animated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self.apiService API_SearchFriend:self withSelector:@selector(outputSearchFriend:) :self.view :searchText];
        });
    }
    else{
        [searchBar resignFirstResponder];
        searchBar.text = @"";
        searchBar.placeholder = JDLocalizedString(@"Search a friend");
        [_searchTableView setHidden:YES];
        [searchBar setShowsCancelButton:NO animated:YES];
    }
}

- (void) outputSearchFriend:(NSDictionary *)responseDict{
    SearchArray = [NSMutableArray new];
    SearchArray = [responseDict valueForKey:@"result"];
    
    if (SearchArray)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            _searchTableView.delegate = self;
            _searchTableView.dataSource = self;
            //  [_searchTableView setHidden:YES];
            [_searchTableView reloadData];
            [_suggestionTableView reloadData];
        });
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [_searchTableView setHidden:YES];
    searchBar.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

