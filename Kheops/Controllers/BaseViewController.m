//
//  JPBaseViewController.m
//  JobProgress
//
//  Created by Vishal on 15/04/16.
//  Copyright © 2016 Logiciel Solutions. All rights reserved.
//

#import "BaseViewController.h"


@interface BaseViewController ()
{
    NSTimer* timer;
}
@end

@implementation BaseViewController
@synthesize  ObjGlobelFucntion, alertController,apiService;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.font = [UIFont fontWithName:@"Roboto-Bold" size:18.0];
    self.whiteColor = [UIColor whiteColor];
    self.grayColor = [UIColor grayColor];
    self.ScreenSize = [[UIScreen mainScreen] bounds];
    ObjGlobelFucntion = [GlobelFunctions new];
    apiService = [CallAPI new];
    _tokenViews = [NSMutableArray new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showErrorMsg) name:@"ShowServerError" object:nil];
}





#pragma mark Flags

- (UIButton *)addRoundRectButtonForButton:(UIButton *)button withHeaderHeight:(CGFloat)headerHeight
{
    float widthIs, heightIs;
    //    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"EDIT" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14.0f];
    
    widthIs =
    [button.titleLabel.text
     boundingRectWithSize:button.frame.size
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{NSFontAttributeName:button.titleLabel.font}
     context:nil]
    .size.width;
    widthIs += 20;
    heightIs = button.frame.size.height;
    button.frame = CGRectMake(self.view.frame.size.width - (widthIs + 10), headerHeight/2 - heightIs, widthIs, heightIs);
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.layer.cornerRadius = button.frame.size.height / 2;
    button.clipsToBounds = YES;
    
    return button;
}

#pragma mark Public


//- (IBAction)startTimer:(id)sender
//{
//    [self performSelectorInBackground:@selector(syncCount) withObject:nil];
//}

- (void)viewDidDisappear:(BOOL)animated
{
}

#pragma mark Globel Functions

//- (void) syncCount
//

//- (UIButton *)loadMoreButtonWithBackGroundColor:(UIColor *)backGroundColor :(UIColor *)titleColor withSelector:(SEL)Selector
//{
//    UIButton *loadMoreBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, self.ScreenSize.size.width - 20, 30)];
//    loadMoreBtn.backgroundColor = backGroundColor;
//    [loadMoreBtn setTitleColor:titleColor forState:UIControlStateNormal];
//    [loadMoreBtn setTitle:NSLocalizedString(@"LoadMore", nil) forState:UIControlStateNormal];
//    [loadMoreBtn addTarget:self action:Selector forControlEvents:UIControlEventTouchUpInside];
//    return loadMoreBtn;
//}


- (void)showErrorMsg
{
    //[self.GlobelFucntion HideLoaderfromView:self.view];
   // [self callToastToComeOutWith:NSLocalizedString(@"whenNoNetwork", nil)];
}

//- (void)callToastToComeOutWith:(NSString *)message
//{
//    [self.navigationController.view makeToast:message duration:LongToastDuration position:nil];
//}

- (void)saveObjects:(NSArray *)objects ToUserDefaultWith:(NSString *)name
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:objects] forKey:name];
    [userDefaults synchronize];
}

- (NSMutableArray *)getValuesFromUserDefaultWith:(NSString *)name
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:name];
    NSDictionary *retrievedDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return [retrievedDictionary mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
