//
//  AppDelegate.h
//  Kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenuShadow.h"
#import <UserNotifications/UserNotifications.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
{
    MFSideMenuContainerViewController *container;
}
@property (strong, nonatomic) UIWindow *window;
@property () BOOL restrictRotation;
@end

