//
//  ViewController.h
//  Kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface LandingViewController : BaseViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSString *fbemailId ;
    NSString *fbname;
    NSString *fbId ;
    NSString *fbPicture, *fbAccessToken;
    
}
@property (strong, nonatomic) IBOutlet UIButton *LoginButton;
@property (strong, nonatomic) IBOutlet UIButton *FacebookButton;
@property (strong, nonatomic) IBOutlet UITextField *EmailTextfield;
@property (strong, nonatomic) IBOutlet UITextField *PasswordTextfield;
@property (strong, nonatomic) IBOutlet UITextField *languageTextfield;
@property (strong, nonatomic) IBOutlet UIButton *SignUpButton;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UIPickerView *dataPicker;
@property (strong, nonatomic) IBOutlet UIButton *DoneButton;
@property (strong, nonatomic) IBOutlet UIButton *forgotButton;
@property (strong, nonatomic) IBOutlet UILabel *labelOr;
@property (strong, nonatomic) IBOutlet UILabel *signUplabel;

@end

