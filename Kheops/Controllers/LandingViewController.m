//
//  ViewController.m
//  kheops
//
//  Created by Tarun Sharma on 08/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import "LandingViewController.h"
#import "HomeController.h"
#import "SignUpController.h"
#import "ProfileController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Foundation/Foundation.h>
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAITracker.h>
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import <GoogleAnalytics/GAITracker.h>
#import <GAITracker.h>
#import <GAI.h>
#import "AFDataManager.h"
#import "LocalizationSystem.h"
#import "ResetPasswordController.h"


@interface LandingViewController (){
    NSArray *DataArray;
}

@end
@implementation LandingViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    _LoginButton.layer.cornerRadius=_LoginButton.frame.size.height/2;
    _LoginButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _LoginButton.layer.borderWidth=1.0f;
    _LoginButton.clipsToBounds = YES;
    
    _FacebookButton.layer.cornerRadius=_FacebookButton.frame.size.height/2;
    _FacebookButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _FacebookButton.layer.borderWidth=1.0f;
    _FacebookButton.clipsToBounds = YES;
    
    [_pickerView setAlpha:0.0f];
}

- (void)viewWillAppear:(BOOL)animated{
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"Login Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Enter your E-mail") textfield:_EmailTextfield];
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Password") textfield:_PasswordTextfield];
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Select Language") textfield:_languageTextfield];
    
    _labelOr.text = JDLocalizedString(@"Or");
    _signUplabel.text = JDLocalizedString(@"Don't have an account? Sign up");
    
    [self.DoneButton setTitle:JDLocalizedString(@"Done") forState:UIControlStateNormal];
    [self.forgotButton setTitle:JDLocalizedString(@"Forgot password?") forState:UIControlStateNormal];
    [self.LoginButton setTitle:JDLocalizedString(@"Login") forState:UIControlStateNormal];
    [self.FacebookButton setTitle:JDLocalizedString(@"Connect Via Facebook") forState:UIControlStateNormal];
    
    NSString *Selectedlanguage = [[NSUserDefaults standardUserDefaults] valueForKey:@"selectedLang"];
    
    if ( [Selectedlanguage isEqualToString:@"en"]){
        _languageTextfield.text = @"English";
    }
//    else if ([Selectedlanguage isEqualToString:@"es"]){
//        _languageTextfield.text = @"Español";
//    }
    else if ([Selectedlanguage isEqualToString:@"fr"]){
        _languageTextfield.text = @"Français";
    }
}

- (IBAction)TappedAction_LoginButton:(id)sender{
    if (!validateEmailWithString(_EmailTextfield.text)){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Please enter your Registered Email Id below.") :self];
    }
    else if (_PasswordTextfield.text.length <=0){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Password Required") :self];
    }
    else if (_languageTextfield.text.length <=0){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Select language") :self];
    }
    else{
        ShowProgressHUD;
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FbLogin"];
        [self.apiService API_UserLogin:self withSelector:@selector(outputLogin:) :self.view :_EmailTextfield.text :_PasswordTextfield.text];
    }
}
- (IBAction)tappedAction_forgotButton:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ResetPasswordController * controller = [storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) outputLogin:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        NSString *name = [[responseDict valueForKey:@"result"] valueForKey:@"name"];
        NSString *email= [[responseDict valueForKey:@"result"] valueForKey:@"email"];
        NSString *userid= [[responseDict valueForKey:@"result"] valueForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults] setValue:email forKey:@"useremail"];
        [[NSUserDefaults standardUserDefaults] setValue:name forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setValue:userid forKey:@"userid"];
        
        NSString *profile_pic= [[responseDict valueForKey:@"result"] valueForKey:@"profile_pic"];
        [[NSUserDefaults standardUserDefaults] setValue:profile_pic forKey:@"profile_pic"];
        
        NSString *token = [[responseDict valueForKey:@"result"] valueForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"fbToken"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            HideProgressHUD;
            [self NavigateToHome];
        });
    }
    else{
        HideProgressHUD;
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Something went wrong.") :self];
    }
}


- (IBAction)TappedAction_loginwithFacebook:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"user_friends",@"public_profile",@"email"] fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     {
         if (error){
             NSLog(@"Process error");
         }
         else if (result.isCancelled){
             NSLog(@"Cancelled");
         }
         else{
             if ([result.grantedPermissions containsObject:@"public_profile"]){
                 fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString ;
                 ShowProgressHUD;
                 [self fetchUserInfo];
             }}
     }];
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken]){
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, picture.width(200).height(200), email "}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error){
                 fbemailId = [result valueForKey:@"email"];
                 fbname = [result valueForKey:@"name"];
                 fbId = [result valueForKey:@"id"];
                 fbPicture = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                 
                 [[NSUserDefaults standardUserDefaults] setValue:fbemailId forKey:@"useremail"];
                 [[NSUserDefaults standardUserDefaults] setValue:fbname forKey:@"username"];
                 [[NSUserDefaults standardUserDefaults] setValue:fbId forKey:@"id"];
                 [[NSUserDefaults standardUserDefaults] setValue:fbPicture forKey:@"profile_pic"];
                 
                 NSString *udid = [[NSUserDefaults standardUserDefaults] valueForKey:@"UDID"];
                 [self.apiService API_LoginWithFacebook:self withSelector:@selector(OutputFBLogin:) :self.view :fbId :fbname :fbemailId :udid :fbPicture :@"iOS"];
             }
             else{
             }
         }];
    }}

- (void) OutputFBLogin:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FbLogin"];
        NSString *name = [[responseDict valueForKey:@"result"] valueForKey:@"name"];
        NSString *email= [[responseDict valueForKey:@"result"] valueForKey:@"email"];
        NSString *userid= [[responseDict valueForKey:@"result"] valueForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults] setValue:email forKey:@"useremail"];
        [[NSUserDefaults standardUserDefaults] setValue:name forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setValue:userid forKey:@"userid"];
        
        NSString *profile_pic= [[responseDict valueForKey:@"result"] valueForKey:@"image"];
        [[NSUserDefaults standardUserDefaults] setValue:profile_pic forKey:@"profile_pic"];
        [[NSUserDefaults standardUserDefaults] setValue:fbAccessToken forKey:@"fbToken"];
      
        NSString *token = [[responseDict valueForKey:@"result"] valueForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            HideProgressHUD;
            [self NavigateToHome];
        });
    }}


- (IBAction)TappedAction_SignUpButton:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) NavigateToHome{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoggedIn"];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeController * controller = [storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)TappedAction_SelectLanguage:(id)sender{
    [_pickerView setAlpha:1.0f];
    _dataPicker.delegate = self;
    _dataPicker.dataSource = self;
   // DataArray = [NSArray arrayWithObjects: @"English", @"Français", @"Español",nil];
    
    DataArray = [NSArray arrayWithObjects: @"English", @"Français",nil];

}

- (IBAction)TappedAction_DoneButton:(id)sender{
    if ([_languageTextfield.text isEqualToString:@"English"]){
        JDLocalizationSetLanguage(@"en");
        [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:@"selectedLang"];
    }
    else if ([_languageTextfield.text isEqualToString:@"Français"]){
        JDLocalizationSetLanguage(@"fr");
        [[NSUserDefaults standardUserDefaults] setValue:@"fr" forKey:@"selectedLang"];
    }
//    else{
//        JDLocalizationSetLanguage(@"es");
//        [[NSUserDefaults standardUserDefaults] setValue:@"es" forKey:@"selectedLang"];
//    }
    [_pickerView setAlpha:0.0f];
    [self viewWillAppear:YES];
}

#pragma mark PickerView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component{
    return DataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component{
    return  [DataArray objectAtIndex: row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    _languageTextfield.text = [DataArray objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *pickerLabel = (UILabel *)view;
    if (pickerLabel == nil){
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[DataArray objectAtIndex: row]];
    return pickerLabel;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end


