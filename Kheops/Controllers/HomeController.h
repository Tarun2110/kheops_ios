//
//  HomeController.h
//  kheops
//
//  Created by Tarun Sharma on 08/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface HomeController : BaseViewController<CLLocationManagerDelegate,UIAlertViewDelegate,MKMapViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate,UITableViewDelegate>
{
    CLLocation *currentLocation;
    IBOutlet UILabel *labelHome;
    
    NSString *UserLat;
    NSString *UserLong;
    
    NSString *ArrUserLat;
    NSString *ArrUserLong;
    NSString *PinId;
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong)  CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UISegmentedControl *tabSegmentController;

@end
