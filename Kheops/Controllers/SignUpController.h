//
//  SignUpController.h
//  kheops
//
//  Created by Tarun Sharma on 08/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SignUpController : BaseViewController<UINavigationControllerDelegate,UIPopoverControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UIImagePickerController *picker;
    NSString *base64String, *fbAccessToken;
}

@property (strong, nonatomic) IBOutlet UITextField *NameTextField;
@property (strong, nonatomic) IBOutlet UITextField *EmailTextField;
@property (strong, nonatomic) IBOutlet UITextField *PasswordTextField;
@property (strong, nonatomic) IBOutlet UIButton *SignUpButton;
@property (strong, nonatomic) IBOutlet UIImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UIPickerView *datapicker;
@property (strong, nonatomic) IBOutlet UIView *pickerView;
@property (strong, nonatomic) IBOutlet UITextField *languageTextfield;
@property (strong, nonatomic) IBOutlet UILabel *labelSignin;
@property (strong, nonatomic) IBOutlet UIButton *facebookButon;
@property (strong, nonatomic) IBOutlet UILabel *labelOr;

@end
