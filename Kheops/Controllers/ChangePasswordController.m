//
//  ChangePasswordController.m
//  kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import "ChangePasswordController.h"
#import <Foundation/Foundation.h>
#import "AFDataManager.h"
#import "LocalizationSystem.h"

@interface ChangePasswordController ()

@end

@implementation ChangePasswordController

- (void)viewDidLoad
{
    _confirmButton.layer.cornerRadius=_confirmButton.frame.size.height/2;
    _confirmButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _confirmButton.layer.borderWidth=1.0f;
    _confirmButton.clipsToBounds = YES;
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated{
    _changePasswordLabel.text = JDLocalizedString(@"Change Password");
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Old Password Required") textfield:_OldPassTextfield];
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"New Password Required") textfield:_newpasstextfield];
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Re-enter New Password") textfield:_confirmpasstextfield];
    [self.confirmButton setTitle:JDLocalizedString(@"Submit") forState:UIControlStateNormal];
}

- (IBAction)TappedAction_Back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)TappedAction_ConfrimButton:(id)sender{
    if (_OldPassTextfield.text.length <=0){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Please enter old password") :self];
    }
    else if (_newpasstextfield.text.length <=0){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Please enter new password") :self];
    }
    else if (_confirmpasstextfield.text.length <=0){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Please confirm new password") :self];
    }
    else if (![_newpasstextfield.text isEqualToString:_confirmpasstextfield.text]){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Password does not match the confirm password.") :self];
    }
    else{
        ShowProgressHUD;
        [self.apiService API_ChangePassword:self withSelector:@selector(outputChangePassword:) :self.view :_OldPassTextfield.text :_newpasstextfield.text];
    }
}

- (void) outputChangePassword:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1)  {
        dispatch_async(dispatch_get_main_queue(), ^{
            HideProgressHUD;
            NSString *token = [[responseDict valueForKey:@"result"] valueForKey:@"token"];
            [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
            [self.ObjGlobelFucntion alert:JDLocalizedString(@"Success") :JDLocalizedString(@"Your password has been changed successfully! Thank you") :self];
        });
    }}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

