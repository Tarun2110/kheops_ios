//
//  SignUpController.m
//  kheops
//
//  Created by Tarun Sharma on 08/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import "SignUpController.h"
#import "HomeController.h"
#import <GoogleAnalytics/GAITracker.h>
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import <GoogleAnalytics/GAITracker.h>
#import <GAITracker.h>
#import <GAI.h>
#import "AFDataManager.h"
#import "LocalizationSystem.h"
#import "LandingViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <AFNetworking/AFNetworking.h>


@interface SignUpController ()
@end
NSArray *dataArray;
NSString* fbemailId;
NSString* fbname;
NSString* fbId;
NSString* fbPicture;
NSData *imageData;
NSMutableDictionary *mutableDict;
NSMutableArray *mutableArrays ;
NSMutableArray *mutableArrayTwo ;
NSMutableArray *mutableArrayQty ;
NSMutableArray *mutableArraycoupon ;

@implementation SignUpController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    _SignUpButton.layer.cornerRadius=_SignUpButton.frame.size.height/2;
    _SignUpButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _SignUpButton.layer.borderWidth=1.0f;
    _SignUpButton.clipsToBounds = YES;
    
    _facebookButon.layer.cornerRadius=_facebookButon.frame.size.height/2;
    _facebookButon.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _facebookButon.layer.borderWidth=1.0f;
    _facebookButon.clipsToBounds = YES;
    
    [_pickerView setAlpha:0.0f];
    
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString *countryCode = [currentLocale objectForKey:NSLocaleLanguageCode];
    
    if ([countryCode isEqualToString:@"fr"]){
        _languageTextfield.text = @"Français";
        JDLocalizationSetLanguage(@"fr");
        [[NSUserDefaults standardUserDefaults] setValue:@"fr" forKey:@"selectedLang"];
    }
//    else if ([countryCode isEqualToString:@"es"]){
//        _languageTextfield.text = @"Español";
//        JDLocalizationSetLanguage(@"es");
//        [[NSUserDefaults standardUserDefaults] setValue:@"es" forKey:@"selectedLang"];
//    }
    else{
        _languageTextfield.text = @"English";
        JDLocalizationSetLanguage(@"en");
        [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:@"selectedLang"];
    }
}


- (void)viewWillAppear:(BOOL)animated{
    
    mutableArrays = [[NSMutableArray alloc]init];
    mutableArrayTwo = [[NSMutableArray alloc]init];
    mutableArrayQty = [[NSMutableArray alloc]init];
    mutableArraycoupon = [[NSMutableArray alloc]init];

    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"SignUp Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Full Name") textfield:_NameTextField];
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Enter your E-mail") textfield:_EmailTextField];
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Password Required") textfield:_PasswordTextField];
    [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Select Language") textfield:_languageTextfield];
    [self.SignUpButton setTitle:JDLocalizedString(@"Sign up") forState:UIControlStateNormal];
    [self.facebookButon setTitle:JDLocalizedString(@"Connect Via Facebook") forState:UIControlStateNormal];
    
    _labelOr.text = JDLocalizedString(@"Or");
    _labelSignin.text = JDLocalizedString(@"Already have an account? Sign In");
   
    // [self TestGroccy1];
}


- (IBAction)TappedAction_FbLogin:(id)sender{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"user_friends",@"public_profile",@"email"] fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
         if (error){
             NSLog(@"Process error");
         }
         else if (result.isCancelled){
             NSLog(@"Cancelled");
         }
         else{
             if ([result.grantedPermissions containsObject:@"public_profile"]){
                 fbAccessToken = [FBSDKAccessToken currentAccessToken].tokenString ;
                 ShowProgressHUD;
                 [self fetchUserInfo];
             }}
     }];
}

-(void)fetchUserInfo{
    if ([FBSDKAccessToken currentAccessToken]){
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, picture.width(200).height(200), email "}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error){
             if (!error){
                 fbemailId = [result valueForKey:@"email"];
                 fbname = [result valueForKey:@"name"];
                 fbId = [result valueForKey:@"id"];
                 fbPicture = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                 
                 [[NSUserDefaults standardUserDefaults] setValue:fbemailId forKey:@"useremail"];
                 [[NSUserDefaults standardUserDefaults] setValue:fbname forKey:@"username"];
                 [[NSUserDefaults standardUserDefaults] setValue:fbId forKey:@"id"];
                 [[NSUserDefaults standardUserDefaults] setValue:fbPicture forKey:@"profile_pic"];
                 
                 ShowProgressHUD;
                 NSString *udid = [[NSUserDefaults standardUserDefaults] valueForKey:@"UDID"];
                 [self.apiService API_LoginWithFacebook:self withSelector:@selector(OutputFBLogin:) :self.view :fbId :fbname :fbemailId :udid :fbPicture :@"iOS"];
             }
             else
             {
             }
         }];
        
}}

- (void) OutputFBLogin:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        [[NSUserDefaults standardUserDefaults] setValue:fbAccessToken forKey:@"fbToken"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FbLogin"];
      
        NSString *name = [[responseDict valueForKey:@"result"] valueForKey:@"name"];
        NSString *email= [[responseDict valueForKey:@"result"] valueForKey:@"email"];
        NSString *userid= [[responseDict valueForKey:@"result"] valueForKey:@"id"];
        
        [[NSUserDefaults standardUserDefaults] setValue:email forKey:@"useremail"];
        [[NSUserDefaults standardUserDefaults] setValue:name forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setValue:userid forKey:@"userid"];
        
        NSString *profile_pic= [[responseDict valueForKey:@"result"] valueForKey:@"image"];
        [[NSUserDefaults standardUserDefaults] setValue:profile_pic forKey:@"profile_pic"];
        
        NSString *token = [[responseDict valueForKey:@"result"] valueForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            HideProgressHUD;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoggedIn"];
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            HomeController * controller = [storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
            [self.navigationController pushViewController:controller animated:YES];
        });
    }
}

- (IBAction)TappedAction_LoginButton:(id)sender{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LandingViewController * controller = [storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
    imageData = UIImageJPEGRepresentation(img,0.5);
    
    _profilePicture.image = [[UIImage alloc]initWithData:imageData];;
    _profilePicture.layer.cornerRadius = _profilePicture.frame.size.height/2;
    _profilePicture.layer.borderWidth = 1.5;
    _profilePicture.layer.masksToBounds = true;
    _profilePicture.contentMode = UIViewContentModeScaleAspectFit;
    _profilePicture.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor blackColor]);
    _profilePicture.clipsToBounds = true;
    
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)Npicker{
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
        [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)TappedAction_SelectLanguage:(id)sender{
    [_pickerView setAlpha:1.0f];
   // dataArray = [NSArray arrayWithObjects: @"English", @"Français", @"Español",nil];
    
    dataArray = [NSArray arrayWithObjects: @"English", @"Français",nil];

    _datapicker.delegate = self;
    _datapicker.dataSource = self;
}

- (IBAction)TappedAction_Done:(id)sender{{
        if ([_languageTextfield.text isEqualToString:@"English"]){
            JDLocalizationSetLanguage(@"en");
            [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:@"selectedLang"];
        }
        else if ([_languageTextfield.text isEqualToString:@"Français"]){
            JDLocalizationSetLanguage(@"fr");
            [[NSUserDefaults standardUserDefaults] setValue:@"fr" forKey:@"selectedLang"];
        }
//        else{
//            JDLocalizationSetLanguage(@"es");
//            [[NSUserDefaults standardUserDefaults] setValue:@"es" forKey:@"selectedLang"];
//        }
    }
    [_pickerView setAlpha:0.0f];
    [self viewWillAppear:YES];
}

- (IBAction)TappedAction_AddPicture:(id)sender{
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:JDLocalizedString(@"Select Photo via")
                                                                                 message:nil
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
       
        UIAlertAction *CameraAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Camera")
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action)
                                       {
                                           picker = [[UIImagePickerController alloc] init];
                                           picker.delegate = self;
                                           picker.allowsEditing = YES;
                                           picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                           [self presentViewController:picker animated:YES completion:NULL];
                                       }];
        
        UIAlertAction *LibraryAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Photo Library")
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action)
                                        {
                                            picker = [[UIImagePickerController alloc] init];
                                            picker.delegate = self;
                                            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                            picker.allowsEditing = YES;
                                            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                            [self presentViewController:picker animated:YES completion:NULL];
                                        }];
        
        UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Cancel")
                                                               style:UIAlertActionStyleDestructive
                                                             handler:^(UIAlertAction *action)
                                       {
                                       }];
        [alertController addAction:CameraAction];
        [alertController addAction:LibraryAction];
        [alertController addAction:CancelAction];
        [alertController setModalPresentationStyle:UIModalPresentationCurrentContext];
        alertController.popoverPresentationController.sourceView = self.view;
        alertController.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.5, self.view.bounds.size.height / 3.8, 1.0, 1.0);
        [self presentViewController:alertController animated:YES
                         completion:nil];
    }
}

- (IBAction)TappedAction_SignUpButton:(id)sender{
    if (isStringEmpty(_NameTextField.text)){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Name Required") :self];
    }
    else if (!validateEmailWithString(_EmailTextField.text)){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Email Required") :self];
    }
    else if (_PasswordTextField.text.length <=0){
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Alert!") :JDLocalizedString(@"Password Required") :self];
    }
    else{
        ShowProgressHUD;
        [self simpleJsonParsingPostMetod];
    }
}


- (void)simpleJsonParsingPostMetod{
    NSString *Udid = [[NSUserDefaults standardUserDefaults] valueForKey:@"UDID"];
    NSString *name = _NameTextField.text;
    NSString *email =  _EmailTextField.text;
    NSString *password = _PasswordTextField.text;
    NSString *device_udid = Udid;
    NSString *device_type = @"ios";
    NSString *image = @"image";
    
    NSString *urlString = [NSString stringWithFormat:@"http://46.101.79.232/api/v1/register"];
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",name] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",email] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"password"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",password] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"device_udid"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",device_udid] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"device_type"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",device_type] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if (imageData){
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"image\"; filename=\"%@\"\r\n",image] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [request setHTTPBody:body];
    NSURLSessionDataTask * dataTask =[[NSURLSession sharedSession]
                                      dataTaskWithRequest:request
                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error){
                                          if(data == nil){
                                              return;
                                          }
                                          NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                          jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              HideProgressHUD;
                                              int  successString = [[jsonResponse valueForKey:@"success"] intValue];
                                              if (successString == 1){
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSideBar" object:nil];
                                                  
                                                  NSString *name = [[jsonResponse valueForKey:@"result"] valueForKey:@"name"];
                                                  NSString *email= [[jsonResponse valueForKey:@"result"] valueForKey:@"email"];
                                                  NSString *userid= [[jsonResponse valueForKey:@"result"] valueForKey:@"id"];
                                                  NSString *ProfilePic= [[jsonResponse valueForKey:@"result"] valueForKey:@"profile_pic"];
                                                  
                                                  [[NSUserDefaults standardUserDefaults] setValue:ProfilePic forKey:@"profile_pic"];
                                                  [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FbLogin"];
                                                  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoggedIn"];
                                                  [[NSUserDefaults standardUserDefaults] setValue:userid forKey:@"userid"];
                                                  [[NSUserDefaults standardUserDefaults] setValue:email forKey:@"useremail"];
                                                  [[NSUserDefaults standardUserDefaults] setValue:name forKey:@"username"];
                                                  
                                                  NSString *token = [[jsonResponse valueForKey:@"result"] valueForKey:@"token"];
                                                  [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
                                                  
                                                  UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                  HomeController * controller = [storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
                                                  [self.navigationController pushViewController:controller animated:YES];
                                              }
                                              else{
                                                  [self.ObjGlobelFucntion alert:@"Alert!" :@"the email has already been registered." :self];
                                              }
                                          });
                                      }];
    [dataTask resume];
}


- (void)TestGroccy1{
    [mutableArrays addObject: @"20"];
   
    //[self Test2];
}


-(void)Test2{
    [mutableArrayTwo addObject: @"unit1"];
    [mutableArrayQty addObject: @"12"];
    [mutableArraycoupon addObject: @"Off50"];

    
    NSDictionary *params =  [NSDictionary dictionaryWithObjectsAndKeys:mutableArrays, @"itemid", mutableArrayTwo, @"unitid",mutableArrayQty, @"qty",mutableArraycoupon, @"coupon",@"5000", @"subtotal",@"200", @"discount",@"11254200", @"custid",@"Testing", @"remarks",@"4546", @"transid",@"Free", @"delivery", @"150", @"wallet", @"5", @"slot",nil];
    
    
    NSLog(@"%@",params);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
  
    [manager POST:@"http://www.groccy.com/new_order_ios.php" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if([responseObject isKindOfClass:[NSDictionary class]]) {
                NSLog(@"%@",responseObject);
            }
            else {
                NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
//                if(success)
             //   {
              
                NSError *error;
               // NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject
                                                                  // options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                                  //   error:&error];
                
               // if (! jsonData) {
                    NSLog(@"Got an error: %@", error);
                //}// else
                {
                NSString *jsonString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"string: %@", jsonString);
                }
                
//                }
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            if(failure) {
//               // failure(error);
//            }
            
        }];
        
    
}


#pragma mark PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component{
    return dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component{
    return  [dataArray objectAtIndex: row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    _languageTextfield.text = [dataArray objectAtIndex:row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *pickerLabel = (UILabel *)view;
    if (pickerLabel == nil){
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[dataArray objectAtIndex: row]];
    return pickerLabel;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end


