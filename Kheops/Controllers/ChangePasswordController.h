//
//  ChangePasswordController.h
//  kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ChangePasswordController : BaseViewController
@property (strong, nonatomic) IBOutlet UITextField *OldPassTextfield;
@property (strong, nonatomic) IBOutlet UITextField *newpasstextfield;
@property (strong, nonatomic) IBOutlet UITextField *confirmpasstextfield;
@property (strong, nonatomic) IBOutlet UIButton *confirmButton;
@property (strong, nonatomic) IBOutlet UILabel *changePasswordLabel;

@end
