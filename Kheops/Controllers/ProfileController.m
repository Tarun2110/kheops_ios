//
//  ProfileController.m
//  kheops
//
//  Created by Tarun Sharma on 08/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//

#import "ProfileController.h"
#import "ChangePasswordController.h"
#import "LocalizationSystem.h"

#import <GoogleAnalytics/GAITracker.h>
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <GoogleAnalytics/GAITracker.h>
#import <GAITracker.h>
#import <GAI.h>
#import "AFDataManager.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKGraphRequest.h>
#import <FBSDKGraphRequestConnection.h>



@interface ProfileController ()
@end
BOOL EditPressed;
int imageSelected ;
NSArray *DataArray;
NSString *Selectlang;
NSData *imgData;

@implementation ProfileController


- (void)viewDidLoad
{
    [super viewDidLoad];
    EditPressed = FALSE;
    imageSelected = 0;
   
    [_SubmitButton setHidden:YES];
    _EmailTextfield.userInteractionEnabled = NO;
    _NameTextField.userInteractionEnabled = YES;
    _AddpictureButton.userInteractionEnabled = YES;
    [_pickerView setAlpha:0.0f];
    _NameTextField.delegate = self;
    
    _ChangePasswordButton.layer.cornerRadius=_ChangePasswordButton.frame.size.height/2;
    _ChangePasswordButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _ChangePasswordButton.layer.borderWidth=1.0f;
    _ChangePasswordButton.clipsToBounds = YES;
    
    _languageButton.layer.cornerRadius=_languageButton.frame.size.height/2;
    _languageButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _languageButton.layer.borderWidth=1.0f;
    _languageButton.clipsToBounds = YES;
    
    _SubmitButton.layer.cornerRadius=_SubmitButton.frame.size.height/2;
    _SubmitButton.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _SubmitButton.layer.borderWidth=1.0f;
    _SubmitButton.clipsToBounds = YES;
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"FbLogin"]){
        [_ChangePasswordButton setHidden:YES];
    }
    else{
        [_ChangePasswordButton setHidden:NO];
    }
    ShowProgressHUD;
}


-(void) viewWillAppear:(BOOL)animated
{
    if (imageSelected == 0){
        NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults ]valueForKey:@"selectedLang"];
        JDLocalizationSetLanguage(selectedLanguage);
        
        if ([selectedLanguage isEqualToString:@"en"]){
            [self.languageButton setTitle:JDLocalizedString(@"English") forState:UIControlStateNormal];
        }
        else if ([selectedLanguage isEqualToString:@"fr"]){
            [self.languageButton setTitle:JDLocalizedString(@"Français") forState:UIControlStateNormal];
        }
//        else{
//            [self.languageButton setTitle:JDLocalizedString(@"Español") forState:UIControlStateNormal];
//        }
        
        [self.ObjGlobelFucntion addBaseLineBelowTextField:JDLocalizedString(@"Name") textfield:_NameTextField ];
        [self.ChangePasswordButton setTitle:JDLocalizedString(@"Change Password") forState:UIControlStateNormal];
        [self.SubmitButton setTitle:JDLocalizedString(@"Submit") forState:UIControlStateNormal];
        
        _profilelabel.text = JDLocalizedString(@"Account Settings");
        [_EditButton setHidden:NO];
        [_languageButton setHidden:NO];
        //  [_SubmitButton setHidden:NO];
        [self.apiService API_GetUserDetails:self withSelector:@selector(outputUserDetail:):self.view];
    }
    else
    {
    }
}



- (void) outputUserDetail:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        HideProgressHUD;
        
        _EmailTextfield.text = [[responseDict valueForKey:@"result"] valueForKey:@"email"] ;
        _NameTextField.text = [[responseDict valueForKey:@"result"] valueForKey:@"name"] ;
        NSString *Fbimage = [[responseDict valueForKey:@"result"] valueForKey:@"image"];
        
        NSString *token = [[responseDict valueForKey:@"result"] valueForKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
        
        if ([Fbimage isEqualToString:@""]){
            _profileImage.image =[UIImage imageNamed:@"placedefault.png"];
        }
        else{
            [_profileImage sd_setImageWithURL:[NSURL URLWithString:Fbimage]];
        }
        _profileImage.contentMode = UIViewContentModeScaleAspectFill;
        _profileImage.layer.cornerRadius = _profileImage.frame.size.height /2;
        _profileImage.layer.masksToBounds = YES;
        _profileImage.layer.borderWidth = 0;
    }
    else{
        HideProgressHUD;
    }
}


-(void)viewDidAppear:(BOOL)animated{
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"Profile Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (IBAction)TappedAction_MenuButton:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSideBar" object:nil];
}

- (IBAction)TappedAction_EditButton:(id)sender{
    if (EditPressed == FALSE){
        [_SubmitButton setHidden:NO];
        _EmailTextfield.userInteractionEnabled = YES;
        _NameTextField.userInteractionEnabled = YES;
        _AddpictureButton.userInteractionEnabled = YES;
        EditPressed = TRUE;
    }
    else{
        EditPressed = FALSE;
        [_SubmitButton setHidden:YES];
        _EmailTextfield.userInteractionEnabled = NO;
        _AddpictureButton.userInteractionEnabled = NO;
        _NameTextField.userInteractionEnabled = NO;
    }
}


- (IBAction)TappedAction_SelectLanguage:(id)sender{
    [_pickerView setAlpha:1.0f];
    _dataPicker.delegate = self;
    _dataPicker.dataSource = self;
 //  DataArray = [NSArray arrayWithObjects: @"English", @"Français", @"Español",nil];
    
    DataArray = [NSArray arrayWithObjects: @"English", @"Français",nil];

}

- (IBAction)TappedAction_Done:(id)sender{
    [_pickerView setAlpha:0.0f];
    if ([Selectlang isEqualToString:@"English"]){
        JDLocalizationSetLanguage(@"en");
        [[NSUserDefaults standardUserDefaults] setValue:@"en" forKey:@"selectedLang"];
    }
    else if ([Selectlang isEqualToString:@"Français"]){
        JDLocalizationSetLanguage(@"fr");
        [[NSUserDefaults standardUserDefaults] setValue:@"fr" forKey:@"selectedLang"];
    }
    else{
        JDLocalizationSetLanguage(@"es");
        [[NSUserDefaults standardUserDefaults] setValue:@"es" forKey:@"selectedLang"];
    }
    [self viewWillAppear:YES];
}


- (IBAction)TappedAction_AddPicture:(id)sender{
    imageSelected = 0;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:JDLocalizedString(@"Select Photo via")
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *CameraAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Camera")
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action)
                                   {
                                       picker = [[UIImagePickerController alloc] init];
                                       picker.delegate = self;
                                       picker.allowsEditing = YES;
                                       picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                       [self presentViewController:picker animated:YES completion:NULL];
                                       
                                   }];
    
    UIAlertAction *LibraryAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Photo Library")
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action)
                                    {
                                        picker = [[UIImagePickerController alloc] init];
                                        picker.delegate = self;
                                        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                        picker.allowsEditing = YES;
                                        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                        [self presentViewController:picker animated:YES completion:NULL];
                                    }];
    
    UIAlertAction *CancelAction = [UIAlertAction actionWithTitle:JDLocalizedString(@"Cancel")
                                                           style:UIAlertActionStyleDestructive
                                                         handler:^(UIAlertAction *action)
                                   {
                                   }];
    [alertController addAction:CameraAction];
    [alertController addAction:LibraryAction];
    [alertController addAction:CancelAction];
    [alertController setModalPresentationStyle:UIModalPresentationCurrentContext];
    alertController.popoverPresentationController.sourceView = self.view;
    alertController.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.5, self.view.bounds.size.height / 3.8, 1.0, 1.0);
    
    [self presentViewController:alertController animated:YES
                     completion:nil];
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *img = [info objectForKey:UIImagePickerControllerEditedImage];
    imgData= UIImageJPEGRepresentation(img,0.5);
   
    _profileImage.image = [[UIImage alloc]initWithData:imgData];;
    _profileImage.layer.cornerRadius = _profileImage.frame.size.height/2;
    _profileImage.layer.borderWidth = 1.5;
    _profileImage.layer.masksToBounds = true;
    _profileImage.contentMode = UIViewContentModeScaleAspectFill;
    _profileImage.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor blackColor]);
    _profileImage.clipsToBounds = true;
    imageSelected = 1;
   
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    [self updateUserInfo];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
        [picker dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)TappedAction_changepassword:(id)sender{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangePasswordController * controller = [storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordController"];
    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)TappedAction_UpdateUser:(id)sender{
    _EmailTextfield.userInteractionEnabled = NO;
    _NameTextField.userInteractionEnabled = NO;
    ShowProgressHUD;
    [self updateUserInfo];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self updateUserInfo];
}

- (void)updateUserInfo{
    NSString *name = _NameTextField.text;
    NSString *image = @"image";
    NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]];
    NSDictionary *headers = @{
                              @"authorization": token,
                              };
    
    NSString *urlString = [NSString stringWithFormat:@"http://46.101.79.232/api/v1/user/editprofile"];
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setAllHTTPHeaderFields:headers];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",@"name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@",name] dataUsingEncoding:NSUTF8StringEncoding]];
    
    if (imgData){
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition:form-data; name=\"image\"; filename=\"%@\"\r\n",image] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imgData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [request setHTTPBody:body];
    
    NSURLSessionDataTask * dataTask =[[NSURLSession sharedSession]
                                      dataTaskWithRequest:request
                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)
                                      {
                                          if(data == nil){
                                              return;
                                          }
                                          NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                          jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              HideProgressHUD;
                                              int  successString = [[jsonResponse valueForKey:@"success"] intValue];
                                              if (successString == 1)
                                              {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self.apiService API_GetUserDetails:self withSelector:@selector(outputUserDetail:) :self.view];
                                                  });
                                              }
                                              else
                                              {
                                              }
                                          });
                                      }];
    [dataTask resume];
}

#pragma mark PickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component{
    return DataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row  forComponent:(NSInteger)component{
    return  [DataArray objectAtIndex: row];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    Selectlang = [DataArray objectAtIndex:row];
    [self.languageButton setTitle:JDLocalizedString(Selectlang) forState:UIControlStateNormal];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = (UILabel *)view;
    if (pickerLabel == nil){
        CGRect frame = CGRectMake(5.0, 0.0, self.view.frame.size.width-10, 32);
        pickerLabel = [[UILabel alloc] initWithFrame:frame];
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    [pickerLabel setText:[DataArray objectAtIndex: row]];
    return pickerLabel;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



@end

