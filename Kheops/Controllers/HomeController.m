//
//  HomeController.m
//  kheops
//
//  Created by Tarun Sharma on 08/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//
#import "ZSAnnotation.h"
#import "pinObject.h"
#import "HomeController.h"
#import "ProfileController.h"
#import "LocationDetailController.h"
#import <GoogleAnalytics/GAITracker.h>
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import <GAITracker.h>
#import <GAI.h>
#import "AFDataManager.h"
#import "LocalizationSystem.h"
#import "CustomTableViewCell.h"
#import "FriendsViewController.h"
#import "ProfileBagdeController.h"
#import "HistoryViewController.h"


@interface HomeController ()
@end

@implementation HomeController
{
    BOOL mapLoaded;
    MKMapCamera *mapCamera;
    NSMutableArray *UnLockedPinArray;
    NSArray *filterdUnlockedArray;
    NSTimer* myTimer;
    NSMutableDictionary *requestDict;
    NSMutableArray *responseArr;
    NSMutableArray *notificationArray;
    UNMutableNotificationContent* content;
    UIView *customView;
}
@synthesize locationManager;
#pragma mark -  ViewSetup

- (void)viewDidLoad{
    [super viewDidLoad];
    notificationArray = [[NSMutableArray alloc] init];
    responseArr = [NSMutableArray new];
    
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"Home Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    labelHome.text =JDLocalizedString(@"Home");
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9){
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(zoomUpPin:) name:@"zoomUpThePin" object:nil];

    CGRect frame= _tabSegmentController.frame;
    [_tabSegmentController setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 32)];

    [_tabSegmentController setTitle:JDLocalizedString(@"Eagles") forSegmentAtIndex:0];
    [_tabSegmentController setTitle:JDLocalizedString(@"Profile") forSegmentAtIndex:1];
    [_tabSegmentController setTitle:JDLocalizedString(@"History") forSegmentAtIndex:2];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mapView.delegate = self;
        [self performSelector:@selector(GetPinsJasss) withObject:nil afterDelay:2.0];
    });
    
}


- (void)zoomUpPin:(NSNotification *)notification {
    NSArray * respone = notification.object;
    double latitude = [[respone valueForKey:@"lat"] doubleValue];
    double longitude = [[respone valueForKey:@"long"] doubleValue];
    
    MKCoordinateRegion region = _mapView.region;
    region.center = CLLocationCoordinate2DMake(latitude,longitude);
    region.span.longitudeDelta= 0.0275f;
    region.span.latitudeDelta= 0.0275f;
    [_mapView setRegion:region animated:YES];
    _mapView.delegate=self;
}

-(void)viewDidAppear:(BOOL)animated{
    [customView removeFromSuperview];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *test = [NSKeyedUnarchiver unarchiveObjectWithData:[userDefaults objectForKey:@"notifyArray"]];
    
    if (test.count>0){
        responseArr = [[NSArray arrayWithArray:test] mutableCopy];
    }
  //  ShowProgressHUD;
  
    myTimer = [NSTimer scheduledTimerWithTimeInterval: 20.0 target: self
                                             selector: @selector(callMeAfterTenSeconds:) userInfo: nil repeats: YES];
    [[NSRunLoop mainRunLoop] addTimer: myTimer forMode:NSRunLoopCommonModes];
}

-(void) viewWillAppear:(BOOL)animated{
    _tabSegmentController.selectedSegmentIndex = -1;
 
    [_tabSegmentController setSelectedSegmentIndex:UISegmentedControlNoSegment];
    mapLoaded = FALSE;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.headingFilter = 1;
    [locationManager startUpdatingLocation];
    locationManager.allowsBackgroundLocationUpdates = YES;
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    if([CLLocationManager locationServicesEnabled]){
        NSLog(@"Location Services Enabled");
    }
    else{
        [self.ObjGlobelFucntion alert:JDLocalizedString(@"Autorisation de la application refusée") :JDLocalizedString(@"Pour ré-activer, accédez à Paramètres et activez le service de localisation pour cette application.") :self];
    }
    self.searchView.alpha = 0.0;
    self.searchBar.placeholder = JDLocalizedString(@"Search a spot");
    _mapView.delegate=self;
    [_mapView setShowsUserLocation:YES];
    _searchBar.delegate = self;
}

- (IBAction)segmentSwitch:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
 
    if (selectedSegment == 0){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];
        FriendsViewController * controller = [storyboard instantiateViewControllerWithIdentifier:@"FriendsViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (selectedSegment == 1){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Tab"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userProfile"];
        ProfileBagdeController * controller = [storyboard instantiateViewControllerWithIdentifier:@"ProfileBagdeController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromTab"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allHistory"];

        HistoryViewController * controller = [storyboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark -  Location Delegates

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:{
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *location = [locations lastObject];
    UserLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    UserLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    if(mapLoaded == FALSE){
        self.mapView.mapType = MKMapTypeStandard;
    }
}

- (void)removeAllPinsButUserLocation1{
    id userLocation = [_mapView userLocation];
    [_mapView removeAnnotations:[_mapView annotations]];
    
    if ( userLocation != nil ) {
        [_mapView addAnnotation:userLocation];
        [_mapView setShowsUserLocation:YES];
    }
}

-(void)ShowUnLockedPinsOnMap{
    NSMutableArray * ann = [NSMutableArray new];
    [self removeAllPinsButUserLocation1];
    
    for (pinObject * p in filterdUnlockedArray) {
        ZSAnnotation *mapPin = [[ZSAnnotation alloc] init];
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(p.pinLat, p.pinLong);
        mapPin.coordinate = coordinate;
        mapPin.pin = p;
        mapPin.title = p.pinTitle;
        [ann addObject:mapPin];
    }
    [self.mapView addAnnotations:ann];
    
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if(mapLoaded == FALSE){
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
        UserLat = [NSString stringWithFormat:@"%f",userLocation.coordinate.latitude];
        UserLong = [NSString stringWithFormat:@"%f",userLocation.coordinate.longitude];
        [self.mapView setRegion:region animated:YES];
        [_mapView setShowsUserLocation:YES];
        mapLoaded = TRUE;
    }
}



- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation{
    if(![annotation isKindOfClass:[ZSAnnotation class]])
        return nil;
    
    ZSAnnotation *a = (ZSAnnotation *)annotation;
    static NSString *defaultPinID = @"StandardIdentifier";
    
    MKAnnotationView *pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinView == nil){
        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    }
    if(a.pin.pinStatus == 1){
        pinView.image = [UIImage imageNamed:@"pin_visited"];
    }
    else{
        pinView.canShowCallout = YES;
        pinView.image = [UIImage imageNamed:@"pin_not_visited"];
    }
    return pinView;
}

-(void)zoomToFitMapAnnotations:(MKMapView*)mapView{
    if([mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(ZSAnnotation* annotation in _mapView.annotations){
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1; // Add a little extra space on the sides
    
    region = [_mapView regionThatFits:region];
    [_mapView setRegion:region animated:YES];
}
#pragma mark  Action Selecting Pin



- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    if(![view.annotation isKindOfClass:[MKUserLocation class]]){
        [mapView deselectAnnotation:view.annotation animated:YES];
        
        [view setCanShowCallout:YES];
        [customView removeFromSuperview];
        
        ZSAnnotation * a =  view.annotation;
        if (a.pin.pinStatus == 1){
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromHistory"];

            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LocationDetailController * controller = [storyboard instantiateViewControllerWithIdentifier:@"LocationDetailController"];
            controller.pin = a.pin;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else{
            if (UserLat){
                customView = [[UIView alloc]initWithFrame:CGRectMake(0,0,180,50)];
                customView.backgroundColor = [UIColor whiteColor];
                customView.layer.cornerRadius = 25;
                customView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                customView.layer.borderWidth = 1.0f;
                customView.layer.masksToBounds = true;
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, customView.frame.size.width-5, 45)];
                titleLabel.text = a.pin.pinTitle;
                titleLabel.numberOfLines = 2;
                [titleLabel setFont:[UIFont fontWithName:@"Museo Sans W01 Rounded 300_0" size:10]];
                [titleLabel setTextAlignment:NSTextAlignmentCenter];
                [titleLabel setAdjustsFontSizeToFitWidth:YES];
                [titleLabel setTextColor:[UIColor blackColor]];
                [customView addSubview:titleLabel];
                
                [view addSubview:customView];
                [self CheckPinStatus:a.pin];
            }
            else{
                     [self.ObjGlobelFucntion alert:JDLocalizedString(@"Autorisation de la application refusée") :JDLocalizedString(@"Pour ré-activer, accédez à Paramètres et activez le service de localisation pour cette application.") :self];
            }}}
}

#pragma mark  SearchBar Delegates

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSMutableArray *arr = [NSMutableArray new];
    if ( [searchBar.text length] > 0 ){
        for (pinObject * p in UnLockedPinArray){
            if ([[p.pinTitle lowercaseString] containsString:[searchBar.text lowercaseString]]){
                [arr addObject:p];
            }}
        filterdUnlockedArray = arr;
        self.searchView.alpha = 1.0;
        _tableView.delegate=self;
        _tableView.dataSource=self;
        [_tableView reloadData];
        [searchBar setShowsCancelButton:YES animated:YES];
    }
    else{
        self.searchView.alpha = 0.0;
        [searchBar resignFirstResponder];
        searchBar.text = @"";
        filterdUnlockedArray = UnLockedPinArray;
        [searchBar setShowsCancelButton:NO animated:YES];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchView.alpha = 0.0;
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    [customView removeFromSuperview];

    filterdUnlockedArray = [NSArray arrayWithArray: UnLockedPinArray ];
    [searchBar setShowsCancelButton:NO animated:YES];
    [self ShowUnLockedPinsOnMap];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
}


- (IBAction)TappedAction_MenuButton:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadSideBar" object:nil];
}

- (IBAction)tappedAction_CurrentLocation:(id)sender
{
    [customView removeFromSuperview];
    MKCoordinateRegion region;
    region.center.latitude = [UserLat doubleValue];
    region.center.longitude = [UserLong doubleValue];
    region.span.longitudeDelta= 0.0275f;
    region.span.latitudeDelta= 0.0275f;
    region = [_mapView regionThatFits:region];
    [_mapView setRegion:region animated:YES];
}

#pragma mark  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return filterdUnlockedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *VideoCellIdentifier = @"CustomPlaceCell";
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:VideoCellIdentifier];
    
    if (cell == nil){
        cell = [[[NSBundle mainBundle ] loadNibNamed:@"CustomTableViewCell" owner:self options:nil]objectAtIndex:1];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    pinObject *pin = [filterdUnlockedArray objectAtIndex:indexPath.row];
    cell.placeTitle.text = pin.pinTitle;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    pinObject *pin = [filterdUnlockedArray objectAtIndex:indexPath.row];
    self.searchView.alpha = 0.0;
    
    _searchBar.text = pin.pinTitle;
    [self ShowFilterData];
    
    MKCoordinateRegion region = _mapView.region;
    region.center = CLLocationCoordinate2DMake(pin.pinLat, pin.pinLong);
    region.span.longitudeDelta= 0.15f;
    region.span.latitudeDelta= 0.15f;
    [_mapView setRegion:region animated:YES];
    _mapView.delegate=self;
}

#pragma mark  Show FilterData on tableView

-(void)ShowFilterData{
    NSMutableArray *arr = [NSMutableArray new];
    if ( [_searchBar.text length] > 0 ){
        for (pinObject * p in UnLockedPinArray){
            if ([[p.pinTitle lowercaseString] containsString:[_searchBar.text lowercaseString]]){
                [arr addObject:p];
            }}
        filterdUnlockedArray = arr;
        self.searchView.alpha = 0.0;
        [_searchBar setShowsCancelButton:NO animated:YES];
    }
    else{
        self.searchView.alpha = 0.0;
        [_searchBar resignFirstResponder];
        _searchBar.text = @"";
        filterdUnlockedArray = UnLockedPinArray;
        [_searchBar setShowsCancelButton:NO animated:YES];
    }
    [self ShowUnLockedPinsOnMap];
}


-(void)addToNotification:(pinObject *)p{
    [notificationArray addObject:p];
}


#pragma mark  Local Notifications setup

-(void) callMeAfterTenSeconds:(NSTimer*)t
{
    for (pinObject * p in UnLockedPinArray){
        if (p.pinStatus == 0){
            CLLocation *placeLocation = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%f",p.pinLat] doubleValue] longitude:[[NSString stringWithFormat:@"%f",p.pinLong] doubleValue]];
            CLLocation *userLocation = [[CLLocation alloc] initWithLatitude:[[NSString stringWithFormat:@"%@",UserLat] doubleValue] longitude:[[NSString stringWithFormat:@"%@",UserLong] doubleValue]];
            CLLocationDistance meters = [placeLocation distanceFromLocation:userLocation];
            
            int totalMeters = meters;
            
            if (totalMeters <= p.pinRadius){
                int index = 0;
                BOOL valuePresent = FALSE;
                for(int j = 0 ; j < responseArr.count ; j++)
                {
                    pinObject * pin = [responseArr objectAtIndex:j];
                    if([pin.pinTitle isEqualToString:p.pinTitle]){
                        valuePresent = TRUE;
                        index = j;
                    }
                }
                if (valuePresent == TRUE){
                    pinObject * piObj = [responseArr objectAtIndex:index];
                    NSDate *CurrentDate =[NSDate date];
                    NSDate *fireDate = piObj.notificationFiredAt;
                    
                    NSTimeInterval distanceBetweenDates = [CurrentDate timeIntervalSinceDate:fireDate];
                    double secondsInAnHour = 3600;
                    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
                    if(hoursBetweenDates >= 48){
                        [self addToNotification:p];
                    }
                    else{
                    }
                }
                else{
                    [self addToNotification:p];
                }
            }
            else{
            }
        }
        else{
        }
    }
    double delayInSeconds = 0.0;
    for(pinObject * pinNotification in notificationArray){
        delayInSeconds += 1.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [self sendLocalNotification:pinNotification];
        });
    }
    [notificationArray removeAllObjects];
}

-(void)sendLocalNotification : (pinObject *)p{
    p.notificationFiredAt = [NSDate date];
    [responseArr addObject:p];
    NSDictionary *parameters = [NSDictionary new];
    parameters = @{ @"place_id": p.pinId,
                    @"lat": [NSNumber numberWithFloat:p.pinLat],
                    @"long":[NSNumber numberWithFloat:p.pinLong] };
    
    NSData *encodedArrayOfPins = [NSKeyedArchiver archivedDataWithRootObject:responseArr];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:encodedArrayOfPins forKey:@"notifyArray"];
    [userDefaults synchronize];
    
    content = [[UNMutableNotificationContent alloc] init];
    
    NSString *WelcomeString = JDLocalizedString(@"Unlock");
    NSString *nowString = JDLocalizedString(@"now");
    
    NSString *String =[NSString stringWithFormat:@"%@ %@ %@ \ue415",WelcomeString,p.pinTitle,nowString];
    content.title = [NSString localizedUserNotificationStringForKey:@"Kheops" arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:String
                                                         arguments:nil];
    content.userInfo = parameters;
    dispatch_async(dispatch_get_main_queue(), ^{
        content.sound = [UNNotificationSound defaultSound];
        content.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    });
    
    UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                  triggerWithTimeInterval:1.0f repeats: NO];
    
    UNNotificationRequest* request = [UNNotificationRequest
                                      requestWithIdentifier:String content:content trigger:trigger];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error != nil) {
        }
    }];
}


#pragma mark  Calling WebServices
-(void) GetPinsJasss{
    NSString *Selectedlanguage = [[NSUserDefaults standardUserDefaults] valueForKey:@"selectedLang"];
    [self.apiService API_GetPlaces:self withSelector:@selector(outputOfPlaces:) :self.view :Selectedlanguage];
}

- (void) outputOfPlaces:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1){
        UnLockedPinArray  = [[NSMutableArray alloc] init];
        HideProgressHUD;
        NSArray* TotalPins = [responseDict valueForKey:@"result"];
        for (int i = 0 ; i < TotalPins.count ; i++){
            pinObject * p = [pinObject new];
            p.pinLat = [[[TotalPins valueForKey:@"latitude"] objectAtIndex:i] floatValue];
            p.pinLong = [[[TotalPins valueForKey:@"longitude"] objectAtIndex:i] floatValue];
            p.pinId = [[TotalPins valueForKey:@"id"] objectAtIndex:i];
            p.pinTitle = [[TotalPins valueForKey:@"name"] objectAtIndex:i];
            p.pinDesc = [[TotalPins valueForKey:@"description"] objectAtIndex:i];
            p.pinImagePath = [[TotalPins valueForKey:@"image"] objectAtIndex:i];
            p.pinAudioPath = [[TotalPins valueForKey:@"file"] objectAtIndex:i];
            p.pinStatus = [[[TotalPins valueForKey:@"status"] objectAtIndex:i] intValue];
            p.pinRadius = [[[TotalPins valueForKey:@"radius"] objectAtIndex:i] intValue];
            p.friendsListArray = [[TotalPins valueForKey:@"friend_list"] objectAtIndex:i];
            p.imagesArray = [[TotalPins valueForKey:@"images"] objectAtIndex:i];
            [UnLockedPinArray addObject:p];
        }
        filterdUnlockedArray = UnLockedPinArray;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self ShowUnLockedPinsOnMap];
        });
    }
}

-(void)CheckPinStatus:(pinObject *)pin{
    NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization": token,
                               };
    
    NSDictionary *parameters = @{ @"place_id": pin.pinId,
                                  @"latitude": UserLat,
                                  @"longitude": UserLong };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    NSString *Selectedlanguage = [[NSUserDefaults standardUserDefaults] valueForKey:@"selectedLang"];
    NSString *urlString = [NSString stringWithFormat:@"http://46.101.79.232/api/v1/map_pin/%@",Selectedlanguage];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                                    if (error){
                                                    }
                                                    else{
                                                        NSDictionary *jsonObject=[NSJSONSerialization
                                                                                  JSONObjectWithData:data
                                                                                  options:NSJSONReadingMutableLeaves
                                                                                  error:nil];
                                                        
                                                        int  successString = [[jsonObject valueForKey:@"success"] intValue];
                                                        if (successString == 1){
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                HideProgressHUD;
                                                                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromHistory"];

                                                                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                LocationDetailController * controller = [storyboard instantiateViewControllerWithIdentifier:@"LocationDetailController"];
                                                                controller.pin = pin;
                                                                controller.popUp = [NSString stringWithFormat:@"%@",[[jsonObject valueForKey:@"result"] valueForKey:@"popup"]];
                                                                controller.popUpAward = [NSString stringWithFormat:@"%@",[[jsonObject valueForKey:@"result"] valueForKey:@"award"]];
                                                                controller.popUpImage = [NSString stringWithFormat:@"%@",[[jsonObject valueForKey:@"result"] valueForKey:@"image"]];
                                                                controller.popUpPlace = [NSString stringWithFormat:@"%@",[[jsonObject valueForKey:@"result"] valueForKey:@"city"]];
                                                                [self.navigationController pushViewController:controller animated:YES];
                                                            });
                                                        }
                                                        else{
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                HideProgressHUD;
                                                                NSString *msg = [jsonObject valueForKey:@"msg"];
                                                                [self.ObjGlobelFucntion alert:nil :msg :self];
                                                            });
                                                        }}
                                                }];
    [dataTask resume];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [_searchBar resignFirstResponder];
}

#pragma mark  DisapperView
-(void)viewDidDisappear:(BOOL)animated{
    [myTimer invalidate];
    myTimer = nil;
    [locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}
@end


