//
//  LocationDetailController.h
//  kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Tarun Sharma. All rights reserved.
//
#import "pinObject.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "BaseViewController.h"


@interface LocationDetailController : BaseViewController <AVAudioPlayerDelegate,UIAlertViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate>{
    NSString *audiofile;
    __weak IBOutlet NSLayoutConstraint *headerHeightInLandscapeConstant;
    __weak IBOutlet NSLayoutConstraint *seekViewHeightConstrainInLanscape;
}
@property (weak, nonatomic) IBOutlet UIImageView *shadowImage;
@property (strong, nonatomic) IBOutlet UILabel *labelheader;
@property (nonatomic, strong) pinObject * pin;
@property (strong, nonatomic) AVPlayer *playera;
@property (strong, nonatomic) AVPlayerLayer *avPlayerLayer;

@property (strong, nonatomic) NSString *popUp;
@property (strong, nonatomic) NSString *popUpImage;
@property (strong, nonatomic) NSString *popUpAward;
@property (strong, nonatomic) NSString *popUpPlace;
@property (weak, nonatomic) IBOutlet UIView *headerBar;
@property (weak, nonatomic) IBOutlet UIView *seekViewBar;
@property (weak, nonatomic) IBOutlet UIView *BannerView;

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UILabel *Pintitlelabel;
@property (strong, nonatomic) IBOutlet UILabel *Description;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UILabel *TotalTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;

@property (strong, nonatomic) IBOutlet UIButton *MuteButton;
@property (strong, nonatomic) IBOutlet UIButton *playbutton;
@property (strong, nonatomic) IBOutlet UIButton *addFriendsLinkButton;

@property (strong, nonatomic) IBOutlet UIImageView *topbannerimage;
@property (strong, nonatomic) IBOutlet UISlider *seekBar;

@property (strong, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UICollectionView *friendsCollectionView;
@property (weak, nonatomic) IBOutlet UIView *emptyArrayView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topbarHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottombarHeight;

@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, strong) NSString * placeId;
@end

