//
//  AFDataManager.m
//  BuildBAAP-Vendor
//
//  Created by satya mahesh kumar on 08/09/16.
//  Copyright © 2016 carsake. All rights reserved.
//

#import "AFDataManager.h"
#import <AFNetworking.h>


@implementation AFDataManager
static  AFDataManager *sharedinstance = nil;
+(AFDataManager*)sharedInstance
{
    if (!sharedinstance)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedinstance=[[AFDataManager alloc]init];
        });
    }
    return sharedinstance;
}

-(void)requestGetApi:(NSString *)urlString parameter:(id)parameter success:(void(^)(id))success failure:(void(^)(NSString *))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"application/xml", nil];
//    manager.requestSerializer.cachePolicy=NSURLRequestReturnCacheDataElseLoad;
    [manager GET:urlString parameters:parameter progress:nil
         success:^(NSURLSessionTask * operation,id responseObject)
     {
         success(responseObject);
     }failure:^(NSURLSessionTask * operation, NSError *error)
     {
         failure(error.localizedDescription);
     }];

}
-(void)requestPostApi:(NSString *)urlString parameter:(id)parameter success:(void(^)(id))success failure:(void(^)(NSString *))failure
{
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:25.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:@{@"Content-Type" : @"application/json",@"Accept" : @"application/json"}];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                                            failure(error.localizedDescription);
                                                        }];
                                                    } else {
                                                        
                                                        id responseObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                                                            NSLog(@"%@",responseObject);
                                                            success(responseObject);
                                                        }];
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
}
+ (void)showAlertTitle:(NSString *)title alert:(NSString *)massage onView:(UIViewController *)Controller
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:massage
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"Hi");
                               }];
    
    [alertController addAction:okAction];
    
    [Controller presentViewController:alertController animated:YES completion:nil];
    
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; 
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



@end
