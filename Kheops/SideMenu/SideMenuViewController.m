//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.


#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "HomeController.h"
#import "ProfileController.h"
#import "AboutController.h"
#import "LandingViewController.h"
#import <FBSDKLoginManager.h>
#import "LocalizationSystem.h"
#import "SignUpController.h"
#import "ProfileBagdeController.h"
#import "FriendsViewController.h"
#import "HistoryViewController.h"


#define IS_IPHONE4 (([[UIScreen mainScreen] bounds].size.height-480)?NO:YES)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)


@implementation SideMenuViewController

#pragma mark - UITableViewDataSource


-(void) viewDidLoad
{
    [super viewDidLoad];
    
    optionsArray = [[NSMutableArray alloc]init];
    
    optionsArray = [NSMutableArray arrayWithObjects:@"",JDLocalizedString(@"Home"),JDLocalizedString(@"Profile"), JDLocalizedString(@"Eagles"),JDLocalizedString(@"About us"), JDLocalizedString(@"Logout"),nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Reload_Funtion:) name:@"ReloadSideBar" object:nil];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView setFrame:CGRectMake(0,0, 500, [UIScreen mainScreen].bounds.size.height + 20)];
    [self.tableView setBounces:NO];
    self.tableView.opaque = NO;
    self.tableView.scrollEnabled = NO;
    self.tableView.alwaysBounceVertical = NO;
    [self.tableView setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:240.0/255.0 blue:124.0/255.0 alpha:1.0]];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void) Reload_Funtion:(NSNotification *)notification
{
    NSString *selectedLanguage = [[NSUserDefaults standardUserDefaults ]valueForKey:@"selectedLang"];
    JDLocalizationSetLanguage(selectedLanguage);
    
    optionsArray = [[NSMutableArray alloc]init];
    
    optionsArray = [NSMutableArray arrayWithObjects:@"",JDLocalizedString(@"Home"),JDLocalizedString(@"Profile"),JDLocalizedString(@"Eagles"), JDLocalizedString(@"About us"),JDLocalizedString(@"History"),JDLocalizedString(@"Account Settings"), JDLocalizedString(@"Logout"),nil];
    
    
    [self.tableView reloadData];
    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return optionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //[self.tableView setSeparatorColor:[UIColor clearColor]];
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (indexPath.row == 0)
    {
        cell =[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        for(UIView *cellView in [cell.contentView subviews])
        {
            [cellView removeFromSuperview];
        }
        cell.accessoryType = NO;
        cell.backgroundColor = [UIColor clearColor];
    }
    else
    {
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        for(UIView *cellView in [cell.contentView subviews])
        {
            [cellView removeFromSuperview];
        }
        
        OptionName = [[UILabel alloc]initWithFrame:CGRectMake(10,0,240,60)];
        OptionName.text = [optionsArray objectAtIndex:indexPath.row];
        [OptionName setFont:[UIFont fontWithName:@"Museo Sans W01 Rounded 300_0" size:13]];
        OptionName.textAlignment = NSTextAlignmentCenter;
        OptionName.textColor = [UIColor blackColor];
        
        
        UIImageView *separatorLine = [[UIImageView alloc] initWithFrame:CGRectMake(10,59, self.view.frame.size.width-20, 1.0f)];
        [separatorLine setBackgroundColor:[UIColor blackColor]];
        
        imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,10,40,40)];
        imv.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imageArray objectAtIndex:indexPath.row]]];
        
        
        [cell.contentView addSubview:separatorLine];
        cell.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:imv];
        [cell.contentView addSubview:OptionName];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        switch ([indexPath row]){
            case 1:{
                HomeController *Obj_ProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:Obj_ProfileVC];
                navigationController.viewControllers = controllers;
                
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
                break;
            case 2:{
                
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userProfile"];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Tab"];

                ProfileBagdeController *Obj_ProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileBagdeController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:Obj_ProfileVC];
                navigationController.viewControllers = controllers;
                
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
                break;
            case 3:{
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];

                FriendsViewController *Obj_ProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendsViewController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:Obj_ProfileVC];
                navigationController.viewControllers = controllers;
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
                break;
                
            case 4:{
                AboutController *Obj_ProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:Obj_ProfileVC];
                navigationController.viewControllers = controllers;
                
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
                break;
                
            case 5:{
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allHistory"];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromTab"];
                HistoryViewController *Obj_ProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:Obj_ProfileVC];
                navigationController.viewControllers = controllers;
                
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
                break;
                
            case 6:{
                ProfileController *Obj_ProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:Obj_ProfileVC];
                navigationController.viewControllers = controllers;
                
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
                break;
            
            case 7:{
                [[FBSDKLoginManager new] logOut];
               // FBSession *session = [FBSession activeSession];
               // [session closeAndClearTokenInformation];
                //[session close];
               // [FBSession setActiveSession:nil];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoggedIn"];
                SignUpController *Obj_ProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:Obj_ProfileVC];
                navigationController.viewControllers = controllers;
                
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
                break;
            default:
                break;
        }
    }}

@end

