//
//  SideMenuViewController.h
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import <UIKit/UIKit.h>

@interface SideMenuViewController : UITableViewController
{
    NSMutableArray *optionsArray;
    NSMutableArray *imageArray;

    UIImageView *imv;
    UIImageView *info;
    UIImageView *imageView ;
    
    UILabel *UserName;
    UILabel *OptionName;

    NSString *EmailId ;
   
    UIImage *image;

    UIButton* SwitchButton;
}
@property (strong, nonatomic) UINavigationController *MainNavigationController;

@end
