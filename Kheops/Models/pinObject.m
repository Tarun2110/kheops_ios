//
//  pinObject.m
//  Kheops
//
//  Created by Rakesh Kumar on 12/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "pinObject.h"

@implementation pinObject
@synthesize pinId,pinLat,pinDesc,pinLong,pinTitle,pinStatus,pinAudioPath,pinImagePath, notificationFiredAt,pinRadius;


- (void)encodeWithCoder:(NSCoder *)encoder
{
    
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.pinId forKey:@"pinId"];
    [encoder encodeFloat:self.pinLat forKey:@"pinLat"];
    [encoder encodeFloat:self.pinLong forKey:@"pinLong"];
    [encoder encodeObject:self.pinDesc forKey:@"pinDesc"];
    [encoder encodeObject:self.pinTitle forKey:@"pinTitle"];
    [encoder encodeInteger:self.pinStatus forKey:@"pinStatus"];
    [encoder encodeInteger:self.pinRadius forKey:@"pinRadius"];
    [encoder encodeObject:self.pinImagePath forKey:@"pinImagePath"];
    [encoder encodeObject:self.notificationFiredAt forKey:@"notificationFiredAt"];
    [encoder encodeObject:self.pinAudioPath forKey:@"pinAudioPath"];
    
    
    
}
- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if( self != nil )
    {
        self.pinAudioPath = [decoder decodeObjectForKey:@"pinAudioPath"];
        self.pinImagePath = [decoder decodeObjectForKey:@"pinImagePath"];
        self.pinRadius = [[decoder decodeObjectForKey:@"pinRadius"] intValue];
        self.pinStatus = [[decoder decodeObjectForKey:@"pinStatus"] intValue];
        self.pinTitle = [decoder decodeObjectForKey:@"pinTitle"];
        self.pinDesc = [decoder decodeObjectForKey:@"pinDesc"];
        self.pinLong = [[decoder decodeObjectForKey:@"pinLong"] floatValue];
        self.pinLat = [[decoder decodeObjectForKey:@"pinLat"] floatValue];
        self.pinId = [decoder decodeObjectForKey:@"pinId"];
        self.notificationFiredAt = [decoder decodeObjectForKey:@"notificationFiredAt"];
        
    }
    return self;
}



@end

