



#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <sys/utsname.h>
#import <MessageUI/MessageUI.h>
#import "ProgressHUD.h"




@interface GlobelFunctions : NSObject<MFMailComposeViewControllerDelegate>

#define HideProgressHUD [ProgressHUD dismiss];
#define ShowProgressHUD [ProgressHUD show:@""];
#define ShowProgressTextHUD [ProgressHUD show:@"Patientez, vous êtes sublime"];

#define ShowDownloading [ProgressHUD show:@"Downloading files..."];
#define Extracting [ProgressHUD show:@"Extracting files..."];
#define ShowProgressSuccess [ProgressHUD showSuccess:nil];
#define ShowProgressSuccessRegister [ProgressHUD showSuccess:@"Invitation has been sent to your registered email."];


@property (nonatomic, strong) UIToolbar* toolbar;

BOOL isStringEmpty(NSString *string);
BOOL isStringEqualZero(NSString *string);
BOOL validateEmailWithString(NSString *emailText);

- (NSString *)getDeviceModelName;

- (NSString *)getCurrentTime;

+ (NSString *)getBaseUrl2;

- (NSString *)getMimeTypeFunction:(NSString *)mimeType;

- (void)makeCallToNumber:(NSString *)phoneNumber;

- (void)showLocationInAppleMap:(NSString *)location;


- (NSString *)changeDateString:(NSString *)dateString;

- (NSString *)changeDateStringWithMiliSeconds:(NSString *)dateString;
- (NSString *)changeDateStringWithMiliMiliSeconds:(NSString *)dateString;

- (NSString *)changeDateStringWithSeconds:(NSString *)dateString;


//- (void)changeDateString:(NSString *)dateString;

//- (void)LocationUpdate;
//- (void)ShowProgress;
//- (void)DismissProgress;

- (void)alert: (NSString *)alertMsg :(NSString *)message :(UIViewController *)TargetController;
- (void)addBaseLineBelowTextField :(NSString *)placeholder textfield:(UITextField *)textfield;
- (void)openMailComposer :(UIViewController *)TargetController;

@end


