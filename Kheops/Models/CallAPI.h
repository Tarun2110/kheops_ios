#import <Foundation/Foundation.h>
#import "GlobelFunctions.h"
#import "Reachability.h"

@class BaseViewController;

@interface CallAPI : NSObject<NSXMLParserDelegate>
{
    __weak NSString *deviceName;
    NSURLConnection *theConnection;
    NSMutableData   *mutResponseData;
    int             intResponseCode;
    Reachability    *reachability;
    NSString *str_currentElement;
    SEL callBackSelector;
    id __weak callBackTarget;
}
@property (strong ,nonatomic) BaseViewController *objBaseVC;
@property (strong,nonatomic) GlobelFunctions *GlobelFucntion;
@property (strong,nonatomic) CallAPI *apiservice;

@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, readwrite) NSInteger internetWorking;
@property (nonatomic) SEL callBackSelector;
@property (nonatomic, weak) id callBackTarget;

- (void)checkInternetConnection;

//API FOR USER AUTHENTICATION//
- (void)API_UserLogin:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email :(NSString *)password ;
- (void)API_VerifyOTP:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)phone_no  :(NSString *)otp;
- (void)API_GetUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)UserId ;
- (void)API_EditUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)email :(NSString *)first_name :(NSString *)last_name :(NSString *)phone_no ;
- (void)API_GetUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView ;
- (void)API_GetFriendList:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)fb_token :(NSString *)language;

- (void)API_GetAwards:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language :(NSString *)userId;

- (void)API_GetPlaces:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language;
- (void)API_SearchFriend:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)data;
- (void)API_AddFriend:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id;
- (void)API_FriendResponse:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)status;

- (void)API_GetAllHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language;
- (void)API_ChangePassword:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)oldPassword :(NSString *)newPassword;
- (void)API_GetPlaceHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language :(NSString *)city_id;

- (void)API_PlaceDetail:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language :(NSString *)place_id;

- (void)API_LoginWithFacebook:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)fbid :(NSString *)name :(NSString *)email :(NSString *)device_udid :(NSString *)image :(NSString *)device_type;


- (void)API_ResetPassword:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email;
- (void)API_AboutUs:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language;

- (void)API_FriendsHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)city_id :(NSString *)friend_id :(NSString *)language;

@end
