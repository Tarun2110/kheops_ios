#import "CallAPI.h"
#import "MPNotificationView.h"

#define BaseUrl @"http://46.101.79.232/api/v1/"

@implementation CallAPI

@synthesize reachability;
@synthesize internetWorking;
@synthesize callBackSelector;
@synthesize callBackTarget;


- (id)init {
    if (self = [super init])
    {
        _GlobelFucntion = [GlobelFunctions new];
    }
    return self;
}

- (void) initAuthorizationFor:(NSString *)methodType
{
}

- (NSString *)setAccessToken
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"AccessToken"];
}




#pragma mark ********************
#pragma mark  Login API Functions
#pragma mark ********************

- (void)API_GetFriendList:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)fb_token :(NSString *)language
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };

         NSDictionary *parameters = @{
                                   @"fb_token": fb_token,
                                   };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@friend/list/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_AboutUs:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@content/aboutus/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
    
}

- (void)API_SearchFriend:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)data
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        
        NSDictionary *parameters = @{
                                     @"data": data,
                                     };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@friend/search",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


- (void)API_ResetPassword:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   };
        
        NSDictionary *parameters = @{
                                   @"email": email,
                                   };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@resetpassword",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_AddFriend:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        
        NSDictionary *parameters = @{
                                     @"user_id": user_id,
                                     };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@friend/add",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_FriendResponse:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)status
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        
        NSDictionary *parameters = @{
                                     @"user_id": user_id,
                                     @"status": status
                                     };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@friend/response",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_UserLogin:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)email :(NSString *)password
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSDictionary *headers =   @{
                                  @"content-type": @"application/json",
                                  };
       
        NSDictionary *parameters = @{
            @"email": email,
            @"password": password
        };
      
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@login",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}


#pragma mark ********************
#pragma mark  Verfify Login API
#pragma mark ********************

- (void)API_VerifyOTP:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)phone_no  :(NSString *)otp
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @{
            @"phone_no": phone_no,
            @"otp": otp
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@verifyotp",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

#pragma mark ********************
#pragma mark  Get User Details API
#pragma mark ********************

- (void)API_GetUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)UserId
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @{
            @"user_id": UserId
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@profile",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_GetUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/profile",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_GetPlaces:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@places/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_GetAwards:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language :(NSString *)userId
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        
        NSDictionary *parameters = @{
            @"user_id": userId,
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/awards/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];

        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_GetAllHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@history/all/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}



- (void)API_GetPlaceHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language :(NSString *)city_id
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        
        
        NSDictionary *parameters = @{ @"city_id": city_id };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@history/place/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_PlaceDetail:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)language :(NSString *)place_id
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        NSDictionary *parameters = @{ @"place_id": place_id };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@place/detail/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_FriendsHistory:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)city_id :(NSString *)friend_id :(NSString *)language
{
    [self checkInternetConnection];
    if (internetWorking == 1)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };
        NSDictionary *parameters = @{
                                     @"city_id": city_id,
                                     @"friend_id": friend_id
                                     };
        
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@history/friends/place/%@",BaseUrl,language]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_ChangePassword:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)oldPassword :(NSString *)newPassword{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSString *token = [NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]] ;
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"authorization": token,
                                   };

        NSDictionary *parameters = @{ @"oldPassword": oldPassword,
                                      @"newPassword": newPassword };
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@user/changePassword",BaseUrl]]];
        request.timeoutInterval = 25.0;
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}



- (void)API_EditUserDetails:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)user_id :(NSString *)email :(NSString *)first_name :(NSString *)last_name :(NSString *)phone_no
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
        NSDictionary *parameters = @{
            @"user_id": user_id,
            @"email": email,
            @"first_name": first_name,
            @"last_name": last_name,
            @"phone_no": phone_no
        };
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@editprofile",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}

- (void)API_LoginWithFacebook:(id)tempTarget withSelector:(SEL)tempSelector :(UIView *)targetView :(NSString *)fbid :(NSString *)name :(NSString *)email :(NSString *)device_udid :(NSString *)image :(NSString *)device_type
{
    [self checkInternetConnection];
    if (internetWorking == 1){
        NSDictionary *headers = @{
                                  @"content-type": @"application/json",
                                  };
     
        NSDictionary *parameters = @{ @"fbid": fbid,
                                      @"name": name,
                                      @"email": email,
                                      @"device_udid": device_udid,
                                      @"image": image,
                                      @"device_type": device_type };
        
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@loginwithfb",BaseUrl]]];
        request.timeoutInterval = 25.0;
        
        [request setHTTPMethod:@"POST"];
        [request setAllHTTPHeaderFields:headers];
        [request setHTTPBody:postData];
        [self handleOutputForOther:request withTarget:tempTarget withSelector:tempSelector];
    }
}



//#pragma mark **********************
//#pragma mark - Output Section
//#pragma mark **********************

- (void)handleOutputForOther:(NSMutableURLRequest *)urlRequest withTarget:(id)tempTarget withSelector:(SEL)tempSelector{
    NSURLSession *session = [NSURLSession sharedSession];
    NSDictionary *dict;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error){
                                              NSLog(@"%@", error);
                                          }
                                          else{
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                              NSMutableDictionary *jsonResponse = [[NSMutableDictionary alloc] init];
                                              jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments                                                                                                            error:&error];
                                              [self showOutPutHereWithTarget:tempTarget withSelector:tempSelector withDictionary:jsonResponse];
                                          }
                                      }];
    [dataTask resume];
}



- (void)showOutPutHereWithTarget:(id)tempTarget withSelector:(SEL)stateSelector  withDictionary:(NSDictionary *)dict{
    [tempTarget performSelectorOnMainThread:stateSelector withObject:dict waitUntilDone:YES];
}

#pragma mark **********************
#pragma mark - Reachability Methods
#pragma mark **********************

- (void)checkInternetConnection{
    reachability = [Reachability reachabilityForInternetConnection];
    [self performSelector:@selector(updateInterfaceWithReachability:)withObject:reachability];
}

- (void)updateInterfaceWithReachability:(Reachability *)curReach{
    if(curReach == reachability){
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        switch (netStatus){
            case NotReachable:{
                internetWorking = 0;
                [MPNotificationView notifyWithText:@"No Network Connection"
                                            detail:@"Connect to a Wi-Fi network or a cellular data."
                                             image:nil
                                          duration:2.0
                                              type:@"Custom"
                                     andTouchBlock:^(MPNotificationView *notificationView)
                 {
                     NSLog( @"Received touch for notification with text: %@", notificationView.textLabel.text );
                 }];
                break;
            }
            case ReachableViaWiFi:{
                internetWorking = 1;
                break;
            }
            case ReachableViaWWAN:{
                internetWorking = 1;
                break;
            }}}
}


-(void)noNetworkAlert{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowServerError" object:nil];
}

@end
