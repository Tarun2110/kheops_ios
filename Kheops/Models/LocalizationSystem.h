//
//  LocalizationSystem.h
//  POC
//
//  Created by Jeroen Trappers on 28/04/12.
//  Copyright (c) 2012 iCapps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#define JDLocalizedString(key) \
[[LocalizationSystem shared] localizedStringForKey:(key) value:@"" table:nil]

#define JDLocalizedStringFromTable(key, tbl, comment) \
[[LocalizationSystem shared] localizedStringForKey:(key) value:@"" table:(tbl)]

#define JDLocalizedStringWithDefaultValue(key, tbl, val, comment) \
[[LocalizationSystem shared] localizedStringForKey:(key) value:(val) table:(tbl)]

#define JDLocalizedViewController(class) \
[[LocalizationSystem shared] localizedViewController:(class)]

#define JDLocalizedImage(name) \
[[LocalizationSystem shared] localizedImage:(name)]

#define JDLocalizationSetLanguage(language) \
[[LocalizationSystem shared] setLanguage:(language)]

#define JDLocalizationReset \
[[LocalizationSystem shared] resetLocalization]

@interface LocalizationSystem : NSObject

+ (LocalizationSystem *)shared;

// gets the string localized
- (NSString *)localizedStringForKey:(NSString *)key
                              value:(NSString *)value
                              table:(NSString *)tableName;

// gets instance of viewcontroller with localized nib.
- (id) localizedViewController: (Class) vcClass;

// gets instance of localized image 
- (UIImage *) localizedImage:(NSString *)name;

// sets the language
- (void) setLanguage:(NSString*) language;
- (NSString *) language;

// resets this system.
- (void) resetLocalization;

@end
