//
//  pinObject.h
//  Kheops
//
//  Created by Rakesh Kumar on 12/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface pinObject : NSObject
@property (nonatomic, strong) NSString * pinTitle, *pinDesc, *pinImagePath, *pinAudioPath, *pinId;
@property  int pinStatus,pinRadius;
@property float pinLat, pinLong;
@property NSDate * notificationFiredAt;
@property NSMutableArray *friendsListArray, *imagesArray;
@end

