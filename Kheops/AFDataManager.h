//
//  AFDataManager.h
//  BuildBAAP-Vendor
//
//  Created by satya mahesh kumar on 08/09/16.
//  Copyright © 2016 carsake. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AFDataManager : NSObject

+(AFDataManager*)sharedInstance;

-(void)requestGetApi:(NSString *)urlString parameter:(id)parameter success:(void(^)(id))success failure:(void(^)(NSString *))failure;
-(void)requestPostApi:(NSString *)urlString parameter:(id)parameter success:(void(^)(id))success failure:(void(^)(NSString *))failure;

+ (void)showAlertTitle:(NSString *)title alert:(NSString *)massage onView:(UIViewController *)Controller;
-(UIColor*)colorWithHexString:(NSString*)hex;
-(BOOL) NSStringIsValidEmail:(NSString *)checkString;
@end
