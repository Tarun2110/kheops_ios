//
//  ServiceHelper.m
//  XPRESSOR
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 10/10/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

#import "ServiceHelper.h"



@implementation ServiceHelper


+ (id)sharedManager
{
    static ServiceHelper *sharedMyManager = nil;
  
    static dispatch_once_t onceToken;
  
    dispatch_once(&onceToken, ^{
      
        sharedMyManager = [[self alloc] init];
   
    });
    
    return sharedMyManager;
}

- (void)setHudShow:(BOOL)show
{
    self.isHudShow = show;
}


- (void)sendRequestWithMethodName:(NSString *)methodName setHTTPMethod:(NSString *)setHTTPMethod params:(NSDictionary *)params completion:(void (^)(NSDictionary *result))completionBlock
{
//    if (![self CheckReachability])
//    {
//        return;
//    }
//    
//    if (self.isHudShow == YES)
//    {
//        [self showHud];
//    }
    
    
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, methodName]]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    if(params)
    {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        
        
        NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        
        NSString *post = [NSString stringWithFormat:@"json=%@", [jsonString  stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        
        
       
        
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        
        [request setHTTPBody:postData];
        
      
        if (![setHTTPMethod isEqualToString:@"login"])
        {
            NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"] ];
            
            if (dict)
            {
                [request setValue:[NSString stringWithFormat:@"%@:%@",[[dict objectForKey:@"user"] objectForKey:@"id"], [[dict objectForKey:@"user"]objectForKey:@"auth_token"]] forHTTPHeaderField:@"Authentication"];
            }
        }
        
    }
    else
    {
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"] ];
        
        if (dict)
        {
            [request setValue:[NSString stringWithFormat:@"%@:%@",[[dict objectForKey:@"user"] objectForKey:@"id"], [[dict objectForKey:@"user"] objectForKey:@"auth_token"]] forHTTPHeaderField:@"Authentication"];
        }
        
    }
  
    [request setHTTPMethod:setHTTPMethod];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [self dismisHud];
         
         
         NSError *error1;
         
         if (data)
         {
             NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             
             NSLog(@"JSON <><> %@", json);
             
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
             
             completionBlock(dict);
         }
     }];
}


/*- (void)sendRequestWithMethodNameForArrayEncoded:(NSString *)methodName setHTTPMethod:(NSString *)setHTTPMethod params:(NSDictionary *)params completion:(void (^)(NSDictionary *result))completionBlock
{
    if (![self CheckReachability])
    {
        return;
    }
    
    if (self.isHudShow == YES)
    {
        [self showHud];
    }
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ServiceURL, @"schedule"]]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
    
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    NSString *post = [NSString stringWithFormat:@"json=%@", [jsonString  stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    
    
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    
    
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey:@"login_dict"] ];
    
    if (dict)
    {
        [request setValue:[NSString stringWithFormat:@"%@:%@",[[dict objectForKey:@"user"] objectForKey:@"id"], [[dict objectForKey:@"user"]objectForKey:@"auth_token"]] forHTTPHeaderField:@"Authentication"];
    }
    
    
    
    [request setHTTPMethod:@"POST"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         NSError *error1;
         
         if (data)
         {
             NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             
             NSLog(@"JSON <><> %@", json);
             
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
             
             completionBlock(dict);
         }
     }];
}*/






@end
