//
//  ServiceHelper.h
//  XPRESSOR
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 10/10/16.
//  Copyright © 2016 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//



#define ServiceURL @"http://kheops-admin.seraphic.tech/apis/"

#define ImageURL @"http://xpressor.net/web"



#import <Foundation/Foundation.h>
//#import "SVProgressHUD.h"


@interface ServiceHelper : NSObject

@property (nonatomic, assign) BOOL isHudShow;


+ (id)sharedManager;


- (void)sendRequestWithMethodName:(NSString *)methodName setHTTPMethod:(NSString *)setHTTPMethod params:(NSDictionary *)params completion:(void (^)(NSDictionary *result))completionBlock;

- (void)sendRequestWithMethodNameForArrayEncoded:(NSString *)methodName setHTTPMethod:(NSString *)setHTTPMethod params:(NSDictionary *)params completion:(void (^)(NSDictionary *result))completionBlock;


- (void)showHud;

- (void)dismisHud;

- (void)setHudShow:(BOOL)show;

@end
