//
//  AboutController.m
//  Kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import "AboutController.h"
#import <GoogleAnalytics/GAITracker.h>
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"
#import "LocalizationSystem.h"

#import <GoogleAnalytics/GAITracker.h>
#import <GAITracker.h>
#import <GAI.h>
@interface AboutController ()

@end

@implementation AboutController

- (void)viewDidLoad {
    [super viewDidLoad];
    ShowProgressHUD;
    NSString *Selectedlanguage = [[NSUserDefaults standardUserDefaults] valueForKey:@"selectedLang"];
    [self.apiService API_AboutUs:self withSelector:@selector(outputAboutUs:) :self.view :Selectedlanguage];
    
}



-(void)viewWillAppear:(BOOL)animated
{
    id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    [tracker set:kGAIScreenName value:@"About Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    _aboutlabel.text = JDLocalizedString(@"About us");
    _paratextview.text = @"";

}

- (void) outputAboutUs:(NSDictionary *)responseDict{
    int  successString = [[responseDict valueForKey:@"success"] intValue];
    if (successString == 1)
    {
        HideProgressHUD;
        
        NSString *htmlString = [responseDict valueForKey:@"message"];
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        _paratextview.attributedText = attributedString;
    }
}

- (IBAction)TappedAction_BackButton:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
