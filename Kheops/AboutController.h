//
//  AboutController.h
//  Kheops
//
//  Created by Rakesh Kumar on 10/09/17.
//  Copyright © 2017 Rakesh Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AboutController : BaseViewController
@property (strong, nonatomic) IBOutlet UILabel *aboutlabel;
@property (strong, nonatomic) IBOutlet UITextView *paratextview;

@end
